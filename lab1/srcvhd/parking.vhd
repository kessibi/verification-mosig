entity Parking is
     port(clk, reset, require_in, avail, timeout1, timeout2, sensor_in,
          insert_ticket, ticketOK, sensor_out
          : in Bit; -- input ports
          gate_in_open, gate_out_open, car_in, ticket_inserted, car_out
          : out Bit); -- output ports 
end Parking;

architecture Automaton of Parking is
    type States is (Idle, Open_in, Close_in, Verif_ticket, Open_out, Close_out);
    signal state, nextstate : States;
begin

    -- transition function
    process(state, require_in, avail, timeout1, timeout2, sensor_in,
            insert_ticket, ticketOK, sensor_out)
    begin
        case state is
            when Idle =>
                if require_in = '1' and avail = '1' and insert_ticket = '0' then
                    nextstate <= Open_in;
                elsif insert_ticket = '1' then nextstate <= Verif_ticket;
                elsif not (require_in  = '1' and avail = '1')
                   and insert_ticket = '0' then
                    nextstate <= state;
                end if;

            when Verif_ticket =>
                if ticketOK = '0' then nextstate <= Idle;
                -- Does this need to an else to avoid a latch?
                elsif ticketOK = '1' then nextstate <= Open_out;
                end if;

            when Open_out =>
                if timeout2 = '1' then nextstate <= Close_out;
                -- timeout2 check is redundant
                elsif timeout2 = '0' and sensor_out = '1' then
                    nextstate <= Close_out;
                -- sensor_out check is redundant
                elsif timeout2 = '0' and sensor_out = '0' then
                    nextstate <= state;
                end if;

            when Close_out => nextstate <= Idle;

            when Open_in =>
                if timeout1 = '0' and sensor_in = '0' then nextstate <= state;
                elsif timeout1 = '0' and sensor_in = '1' then nextstate <= Close_in;
                elsif timeout1 = '1' then nextstate <= Close_in;
                end if;

            when Close_in => nextstate <= Idle;
        end case;
    end process;

    -- update state
    process(clk, reset)
    begin
        if reset = '1' then
            state <= Idle;
        elsif clk'event and clk = '1' then
            state <= nextstate;
        end if;
    end process;

    -- output function
    process(state, require_in, avail, timeout1, timeout2, sensor_in,
            insert_ticket, ticketOK, sensor_out)
    begin
        -- ticket_inserted
        if state = Idle and insert_ticket = '1' then
            ticket_inserted <= '1';
        else ticket_inserted <= '0';
        end if;

        -- gate_in_open
        if state = Idle and require_in = '1' and avail = '1'
           and insert_ticket = '0' then
            gate_in_open <= '1';
        elsif state = Open_in and timeout1 = '1' then
            gate_in_open <= '0';
        elsif state = Open_in and timeout1 = '0' and sensor_in = '1' then
            gate_in_open <= '0';
        else
            gate_in_open <= '0';
        end if;

        -- car_in
        if state = Open_in and timeout1 = '0' and sensor_in = '1' then
            car_in <= '1';
        else
            car_in <= '0';
        end if;

        -- gate_out_open
        if state = Verif_ticket and ticketOK = '1' then
            gate_out_open <= '1';
        elsif state = Open_out and timeout2 = '1' then
            gate_out_open <= '0';
        elsif state = Open_out and timeout2 = '0' and sensor_out = '1' then
            gate_out_open <= '0';
        else
            gate_out_open <= '0';
        end if;

        -- car_out
        if state = Open_out and timeout2 = '0' and sensor_out = '1' then
            car_out <= '1';
        else
            car_out <= '0';
        end if;
    end process;

end Automaton;
