entity testParking is
end testParking;

architecture test of testParking is
  component Parking is
     port(clk, reset, require_in, avail, timeout1, timeout2, sensor_in,
          insert_ticket, ticketOK, sensor_out
          : in Bit;
          gate_in_open, gate_out_open, car_in, ticket_inserted, car_out
          : out Bit);
  end component;
  signal ck, rst, rqin, av, tm1, tm2, sin, it, tok, sout : bit;
  signal gio, goo, ci, ti : bit;
begin
     P: Parking port map(ck, rst, rqin, av, tm1, tm2, sin, it, tok, sout,
        gio, goo, ci, ti); -- output ports

     -- Add here your input stimulis
     ck <= not ck after 20 ns;
     rst <= '1', '0' after 5 ns;

     rqin <= '0', '1' after 15 ns, '0' after 35 ns;
     av   <= '1';
     tm1  <= '0', '0'  after 35 ns;
     tm2  <= '0', '0' after 175 ns;
     sin  <= '0', '1' after 35 ns;
     it   <= '0', '1' after 135 ns, '0' after 295 ns;
     tok  <= '0', '1' after 155 ns;
     sout <= '0', '1' after 175 ns;
end test;

library LIB_PARKING;
library LIB_PARKING_BENCH;

configuration config1 of LIB_PARKING_BENCH.testParking is 
    for test
       for P: Parking use entity LIB_PARKING.Parking(Automaton);
       end for;
    end for; 
end config1; 
