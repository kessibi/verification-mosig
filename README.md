# VHDL Project - Verification course

## Notes
    - system  is  synchronized  by  the  rising  edges  of  the  clock clk
    - asynchronous (active  high) reset


## States
    - Idle: initial state, state after reset
    - Resting: automatically reached after Idle
    - Randomwalk
    - Movetofood
    - Scanarea
    - Homing
    - Grabfood
    - Movetohome
    - Deposit


## Inputs
    - aboverestth: '1' when time in resting exceeds threshold
    - abovesearchth: '1' time spent on search exceeds threshold
    - 

## Transitions
    - idle -> Resting (unconditionally)
    - Resting -> Randomwalk if aboverestth is '1'
    - Randomwalk -> Homing if abovesearchth is '1'
    - Randomwalk -> Movetofood if findfood is '1'
    - Movetofood -> Homing if abovesearchth is '1'
    - Movetofood -> Scnarena if lostfood is '1'
    - Movetofood -> Grabfood if closetofood is '1'
    - Scanarena -> Homing if abovesearchth is '1'
    - Scanarena -> Movetofood if findfood is '1'
    - Scanarena -> Randomwalk if scantimeup is '1'
    - Grabfood -> Movetohome if success is '1', "as soon as", might not depend
        on the clock but it is assumed it does
    - movetohome -> deposit if athome is '1'
    - deposit -> resting if success is '1'

## Loops
    - Idle: NO loop since "which leads to state resting in the next cycle."
    - Resting: loop on not(aboverestth)
    - Randomwalk: loop on not(findfood or abovesearchth)
    - Movetofood: not(closetofood or abovesearchth)
    - Scanarena: not(findfood or scantimeup or abovesearchth)
    - Homing: NO
    - Grabfood: not(success)
    - Movetohome: not(athome)
    - Deposit: not(success)

## Output
    rest: '1' by _ -> resting (except loopback)
    search: '1' by transition resting -> randomwalk
    food: '1' by transition to grabfood


