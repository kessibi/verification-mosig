Coverage Report Summary Data by file

=================================================================================
=== File: /tp/xm1iarc/xm1iarc006/project/srcvhd/robot.vhd
=================================================================================
    Enabled Coverage            Active      Hits    Misses % Covered
    ----------------            ------      ----    ------ ---------
    Stmts                           32        16        16      50.0
    Branches                        38        17        21      44.7
    FSMs                                                        31.8
        States                       9         4         5      44.4
        Transitions                 31         6        25      19.3


TOTAL ASSERTION COVERAGE: 40.0%  ASSERTIONS: 5

Total Coverage By File (code coverage only, filtered view): 42.2%

