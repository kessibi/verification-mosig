
-- 
-- Definition of  Counter
-- 
--      Tue Apr 21 19:11:11 2020
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Counter is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      start : IN std_logic ;
      aboveth : OUT std_logic) ;
end Counter ;

architecture Behav of Counter is
   signal aboveth_EXMPLR, count_3, state, nx8, nx18, count_2, count_1, 
      count_0, nx36, nx52, nx68, nx84, nx206, nx211, nx213, nx215, nx217, 
      nx221, nx223, nx225, nx228, nx230, nx233, nx236, nx238, nx240, nx244, 
      nx246, nx248, nx250: std_logic ;

begin
   aboveth <= aboveth_EXMPLR ;
   ix93 : OAI311 port map ( Q=>aboveth_EXMPLR, A=>nx206, B=>nx225, C=>nx230, 
      D=>nx246);
   ix53 : OAI311 port map ( Q=>nx52, A=>nx206, B=>state, C=>start, D=>nx217
   );
   ix212 : CLKIN1 port map ( Q=>nx211, A=>start);
   reg_state : DFC1 port map ( Q=>state, QN=>nx213, C=>clk, D=>nx8, RN=>
      nx215);
   ix216 : CLKIN1 port map ( Q=>nx215, A=>reset);
   ix218 : OAI2111 port map ( Q=>nx217, A=>count_0, B=>count_1, C=>nx228, D
      =>nx8);
   ix37 : OAI221 port map ( Q=>nx36, A=>state, B=>nx221, C=>aboveth_EXMPLR, 
      D=>nx223);
   ix224 : NAND21 port map ( Q=>nx223, A=>nx225, B=>state);
   reg_count_0 : DFC1 port map ( Q=>count_0, QN=>nx225, C=>clk, D=>nx36, RN
      =>nx215);
   reg_count_1 : DFC1 port map ( Q=>count_1, QN=>nx206, C=>clk, D=>nx52, RN
      =>nx215);
   ix229 : NAND21 port map ( Q=>nx228, A=>count_1, B=>count_0);
   ix69 : OAI311 port map ( Q=>nx68, A=>nx233, B=>nx236, C=>nx240, D=>nx250
   );
   ix234 : AOI211 port map ( Q=>nx233, A=>count_1, B=>count_0, C=>count_2);
   reg_count_2 : DFC1 port map ( Q=>count_2, QN=>nx230, C=>clk, D=>nx68, RN
      =>nx215);
   ix239 : NOR21 port map ( Q=>nx238, A=>nx240, B=>count_3);
   ix241 : NOR31 port map ( Q=>nx240, A=>nx206, B=>nx225, C=>nx230);
   ix85 : OAI221 port map ( Q=>nx84, A=>nx244, B=>nx236, C=>nx246, D=>nx248
   );
   reg_count_3 : DFC1 port map ( Q=>count_3, QN=>nx246, C=>clk, D=>nx84, RN
      =>nx215);
   ix249 : NAND21 port map ( Q=>nx248, A=>nx213, B=>nx211);
   ix251 : NAND21 port map ( Q=>nx250, A=>count_2, B=>nx18);
   ix19 : NOR21 port map ( Q=>nx18, A=>state, B=>start);
   ix9 : IMUX21 port map ( Q=>nx8, A=>nx211, B=>aboveth_EXMPLR, S=>state);
   ix222 : XOR21 port map ( Q=>nx221, A=>nx225, B=>start);
   ix237 : IMUX21 port map ( Q=>nx236, A=>start, B=>nx238, S=>state);
   ix245 : XOR21 port map ( Q=>nx244, A=>nx246, B=>nx240);
end Behav ;

