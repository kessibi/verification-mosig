
-- 
-- Definition of  Robot
-- 
--      Wed 15 Apr 2020 11:38:53 AM CEST
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Robot is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end Robot ;

architecture Bot of Robot is
   signal rest_EXMPLR, food_EXMPLR, state_1, state_0, nx488, nx10, state_3, 
      nx26, state_2, nx40, nx50, nx490, nx56, nx76, nx82, nx92, nx104, nx118, 
      nx120, nx140, nx148, nx174, nx498, nx501, nx504, nx507, nx510, nx514, 
      nx516, nx519, nx522, nx526, nx529, nx532, nx534, nx537, nx540, nx544, 
      nx546, nx550, nx553, nx555, nx557, nx559, nx562, nx565, nx567, nx570: 
   std_logic ;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix185 : CLKIN1 port map ( Q=>food_EXMPLR, A=>nx498);
   ix499 : NAND31 port map ( Q=>nx498, A=>nx488, B=>closetofood, C=>state_0
   );
   ix181 : NOR21 port map ( Q=>nx488, A=>nx501, B=>nx544);
   ix175 : OAI2111 port map ( Q=>nx174, A=>food_EXMPLR, B=>nx504, C=>nx565, 
      D=>nx567);
   ix505 : OAI2111 port map ( Q=>nx504, A=>nx26, B=>state_2, C=>nx519, D=>
      state_1);
   ix27 : NAND21 port map ( Q=>nx26, A=>nx507, B=>athome);
   ix149 : OAI2111 port map ( Q=>nx148, A=>lostfood, B=>nx510, C=>nx516, D=>
      nx559);
   ix511 : AOI211 port map ( Q=>nx510, A=>nx488, B=>nx10, C=>food_EXMPLR);
   ix11 : OAI211 port map ( Q=>nx10, A=>abovesearchth, B=>nx507, C=>state_1
   );
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx501, C=>clk, D=>nx174, RN
      =>nx514);
   ix515 : CLKIN1 port map ( Q=>nx514, A=>reset);
   ix517 : AOI221 port map ( Q=>nx516, A=>success, B=>nx490, C=>nx40, D=>
      nx140);
   ix99 : NOR21 port map ( Q=>nx490, A=>nx519, B=>nx534);
   ix105 : OAI221 port map ( Q=>nx104, A=>nx26, B=>nx522, C=>success, D=>
      nx526);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx501, B=>nx519, C=>state_2);
   ix93 : OAI2111 port map ( Q=>nx92, A=>success, B=>nx526, C=>nx498, D=>
      nx529);
   ix527 : NAND21 port map ( Q=>nx526, A=>state_3, B=>state_2);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx519, C=>clk, D=>nx104, RN
      =>nx514);
   ix530 : AOI311 port map ( Q=>nx529, A=>nx50, B=>findfood, C=>nx537, D=>
      nx82);
   ix51 : OAI221 port map ( Q=>nx50, A=>nx501, B=>nx532, C=>state_3, D=>
      state_0);
   ix533 : NAND21 port map ( Q=>nx532, A=>nx519, B=>nx534);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx534, C=>clk, D=>nx92, RN
      =>nx514);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx507, C=>clk, D=>nx148, RN
      =>nx514);
   ix538 : OAI211 port map ( Q=>nx537, A=>nx56, B=>nx40, C=>nx507);
   ix57 : NOR40 port map ( Q=>nx56, A=>nx519, B=>nx534, C=>nx540, D=>state_1
   );
   ix541 : CLKIN1 port map ( Q=>nx540, A=>success);
   ix41 : NOR21 port map ( Q=>nx40, A=>state_3, B=>state_2);
   ix83 : NOR21 port map ( Q=>nx82, A=>nx544, B=>nx546);
   ix545 : NAND21 port map ( Q=>nx544, A=>nx519, B=>state_2);
   ix547 : AOI2111 port map ( Q=>nx546, A=>lostfood, B=>state_0, C=>nx76, D
      =>nx10);
   ix77 : NOR21 port map ( Q=>nx76, A=>abovesearchth, B=>scantimeup);
   ix141 : OAI2111 port map ( Q=>nx140, A=>nx550, B=>rest_EXMPLR, C=>nx555, 
      D=>nx26);
   ix551 : CLKIN1 port map ( Q=>nx550, A=>findfood);
   ix63 : NOR21 port map ( Q=>rest_EXMPLR, A=>state_0, B=>nx553);
   ix554 : AOI311 port map ( Q=>nx553, A=>nx490, B=>success, C=>nx501, D=>
      nx40);
   ix556 : AOI211 port map ( Q=>nx555, A=>nx557, B=>state_0, C=>nx501);
   ix558 : CLKIN1 port map ( Q=>nx557, A=>abovesearchth);
   ix560 : AOI311 port map ( Q=>nx559, A=>nx120, B=>nx507, C=>nx488, D=>
      nx118);
   ix121 : NAND21 port map ( Q=>nx120, A=>nx562, B=>nx550);
   ix563 : CLKIN1 port map ( Q=>nx562, A=>scantimeup);
   ix119 : NOR40 port map ( Q=>nx118, A=>success, B=>nx507, C=>nx544, D=>
      state_1);
   ix566 : NAND21 port map ( Q=>nx565, A=>lostfood, B=>food_EXMPLR);
   ix568 : NAND41 port map ( Q=>nx567, A=>aboverestth, B=>nx501, C=>state_0, 
      D=>nx40);
   ix157 : NOR40 port map ( Q=>search, A=>nx570, B=>nx507, C=>state_3, D=>
      state_2);
   ix571 : NAND21 port map ( Q=>nx570, A=>aboverestth, B=>nx501);
end Bot ;

