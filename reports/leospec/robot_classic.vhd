
-- 
-- Definition of  Robot
-- 
--      Wed 15 Apr 2020 11:21:45 AM CEST
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library c35_CORELIB;
use c35_CORELIB.vcomponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Robot is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end Robot ;

architecture Bot of Robot is
   signal rest_EXMPLR, food_EXMPLR, state_2, state_0, state_1, nx465, nx14, 
      nx466, nx24, nx38, nx60, nx74, state_3, nx100, nx116, nx138, nx148, 
      nx164, nx170, nx180, nx186, nx474, nx477, nx480, nx482, nx485, nx491, 
      nx494, nx496, nx498, nx503, nx505, nx507, nx509, nx512, nx514, nx516, 
      nx519, nx522, nx525, nx527, nx529, nx532, nx538, nx540, nx542, nx545: 
   std_logic ;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix203 : NOR40 port map ( Q=>food_EXMPLR, A=>nx474, B=>state_1, C=>nx542, 
      D=>nx505);
   ix191 : OAI2111 port map ( Q=>nx465, A=>nx477, B=>nx532, C=>nx485, D=>
      nx540);
   ix478 : NAND21 port map ( Q=>nx477, A=>state_2, B=>nx482);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx474, C=>clk, D=>nx465, RN
      =>nx480);
   ix481 : CLKIN1 port map ( Q=>nx480, A=>reset);
   ix39 : OAI2111 port map ( Q=>nx38, A=>lostfood, B=>nx485, C=>nx522, D=>
      nx527);
   ix486 : NAND31 port map ( Q=>nx485, A=>nx466, B=>closetofood, C=>state_0
   );
   ix199 : NOR21 port map ( Q=>nx466, A=>nx474, B=>state_1);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx482, C=>clk, D=>nx38, RN
      =>nx480);
   ix149 : NAND41 port map ( Q=>nx148, A=>nx491, B=>nx498, C=>nx519, D=>
      nx525);
   ix492 : OAI211 port map ( Q=>nx491, A=>nx138, B=>nx474, C=>nx465);
   ix139 : OAI311 port map ( Q=>nx138, A=>state_0, B=>nx494, C=>state_1, D=>
      nx496);
   ix495 : CLKIN1 port map ( Q=>nx494, A=>findfood);
   ix497 : OAI211 port map ( Q=>nx496, A=>state_0, B=>success, C=>state_1);
   ix499 : AOI311 port map ( Q=>nx498, A=>state_0, B=>nx474, C=>state_1, D=>
      nx116);
   ix117 : AOI2111 port map ( Q=>nx116, A=>state_3, B=>nx516, C=>state_0, D
      =>nx24);
   ix101 : AOI211 port map ( Q=>nx100, A=>nx503, B=>nx509, C=>rest_EXMPLR);
   ix504 : NAND21 port map ( Q=>nx503, A=>nx505, B=>nx507);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx505, C=>clk, D=>nx148, RN
      =>nx480);
   ix508 : NOR21 port map ( Q=>nx507, A=>state_1, B=>state_2);
   ix510 : NAND31 port map ( Q=>nx509, A=>state_0, B=>athome, C=>state_1);
   ix125 : OAI311 port map ( Q=>rest_EXMPLR, A=>nx505, B=>state_2, C=>nx482, 
      D=>nx512);
   ix513 : OAI2111 port map ( Q=>nx512, A=>nx514, B=>success, C=>nx505, D=>
      nx507);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx514, C=>clk, D=>nx100, RN
      =>nx480);
   ix517 : CLKIN1 port map ( Q=>nx516, A=>success);
   ix25 : NAND21 port map ( Q=>nx24, A=>nx482, B=>nx474);
   ix520 : AOI311 port map ( Q=>nx519, A=>nx74, B=>state_0, C=>nx522, D=>
      nx60);
   ix75 : OAI311 port map ( Q=>nx74, A=>food_EXMPLR, B=>lostfood, C=>state_1, 
      D=>state_2);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx507, B=>aboverestth, C=>state_0
   );
   ix61 : NOR40 port map ( Q=>nx60, A=>scantimeup, B=>nx474, C=>state_0, D=>
      nx465);
   ix526 : NAND31 port map ( Q=>nx525, A=>state_1, B=>abovesearchth, C=>
      nx474);
   ix528 : AOI221 port map ( Q=>nx527, A=>nx529, B=>nx466, C=>state_1, D=>
      nx14);
   ix530 : NOR40 port map ( Q=>nx529, A=>nx186, B=>food_EXMPLR, C=>nx170, D
      =>nx164);
   ix187 : NOR21 port map ( Q=>nx186, A=>nx477, B=>nx532);
   ix533 : AOI211 port map ( Q=>nx532, A=>lostfood, B=>state_0, C=>nx180);
   ix181 : AOI211 port map ( Q=>nx180, A=>nx505, B=>scantimeup, C=>
      abovesearchth);
   ix171 : NOR31 port map ( Q=>nx170, A=>nx507, B=>nx494, C=>state_0);
   ix165 : AOI2111 port map ( Q=>nx164, A=>state_0, B=>athome, C=>nx474, D=>
      nx482);
   ix15 : OAI211 port map ( Q=>nx14, A=>athome, B=>nx505, C=>nx538);
   ix541 : AOI311 port map ( Q=>nx540, A=>nx24, B=>findfood, C=>nx505, D=>
      nx164);
   ix543 : CLKIN1 port map ( Q=>nx542, A=>closetofood);
   ix31 : NOR40 port map ( Q=>search, A=>state_1, B=>state_2, C=>nx545, D=>
      nx505);
   ix546 : CLKIN1 port map ( Q=>nx545, A=>aboverestth);
   ix539 : MUX22 port map ( Q=>nx538, A=>state_0, B=>nx474, S=>nx465);
end Bot ;

