
-- 
-- Definition of  Robot
-- 
--      Wed 15 Apr 2020 11:32:44 AM CEST
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Robot is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end Robot ;

architecture Bot of Robot is
   signal nx720, state_1, state_0, state_8, state_5, state_4, nx28, nx32, 
      state_2, nx50, nx60, nx76, nx88, nx100, state_3, nx120, nx126, nx140, 
      nx150, nx739, nx741, nx744, nx748, nx750, nx752, nx755, nx758, nx762, 
      nx764, nx766, nx770, nx773, nx775, nx778, nx779, nx782, nx784, nx788, 
      nx790, nx793, nx795, nx798, nx800: std_logic ;

begin
   ix721 : TIE0 port map ( Q=>nx720);
   ix157 : NOR21 port map ( Q=>search, A=>nx739, B=>nx741);
   ix740 : CLKIN1 port map ( Q=>nx739, A=>aboverestth);
   ix151 : OAI2111 port map ( Q=>nx150, A=>aboverestth, B=>nx741, C=>nx744, 
      D=>nx784);
   ix745 : NAND21 port map ( Q=>nx744, A=>athome, B=>state_3);
   ix141 : OAI221 port map ( Q=>nx140, A=>nx748, B=>nx750, C=>athome, D=>
      nx782);
   ix749 : CLKIN1 port map ( Q=>nx748, A=>abovesearchth);
   ix751 : AOI211 port map ( Q=>nx750, A=>nx752, B=>nx126, C=>nx120);
   ix753 : CLKIN1 port map ( Q=>nx752, A=>findfood);
   ix127 : OAI211 port map ( Q=>nx126, A=>scantimeup, B=>nx755, C=>nx778);
   ix33 : CLKIN1 port map ( Q=>nx32, A=>nx758);
   ix759 : AOI211 port map ( Q=>nx758, A=>lostfood, B=>state_5, C=>nx28);
   reg_state_5 : DFC1 port map ( Q=>state_5, QN=>nx779, C=>clk, D=>nx60, RN
      =>nx773);
   ix61 : OAI311 port map ( Q=>nx60, A=>lostfood, B=>abovesearchth, C=>nx762, 
      D=>nx766);
   ix763 : NAND21 port map ( Q=>nx762, A=>nx764, B=>state_5);
   ix765 : CLKIN1 port map ( Q=>nx764, A=>closetofood);
   ix767 : OAI211 port map ( Q=>nx766, A=>state_2, B=>state_4, C=>findfood);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx778, C=>clk, D=>nx50, RN
      =>nx773);
   ix51 : OAI211 port map ( Q=>nx50, A=>findfood, B=>nx770, C=>nx775);
   ix771 : AOI221 port map ( Q=>nx770, A=>scantimeup, B=>state_4, C=>nx748, 
      D=>state_2);
   reg_state_4 : DFC1 port map ( Q=>state_4, QN=>nx755, C=>clk, D=>nx32, RN
      =>nx773);
   ix774 : CLKIN1 port map ( Q=>nx773, A=>reset);
   ix776 : NAND21 port map ( Q=>nx775, A=>aboverestth, B=>state_1);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx741, C=>clk, D=>nx150, RN
      =>nx773);
   ix29 : NOR40 port map ( Q=>nx28, A=>scantimeup, B=>abovesearchth, C=>
      findfood, D=>nx755);
   ix121 : NOR31 port map ( Q=>nx120, A=>nx779, B=>lostfood, C=>closetofood
   );
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx782, C=>clk, D=>nx140, RN
      =>nx773);
   ix785 : AOI211 port map ( Q=>nx784, A=>success, B=>state_8, C=>state_0);
   ix101 : OAI221 port map ( Q=>nx100, A=>nx788, B=>nx790, C=>success, D=>
      nx800);
   ix789 : CLKIN1 port map ( Q=>nx788, A=>athome);
   reg_state_7 : DFC1 port map ( Q=>OPEN, QN=>nx790, C=>clk, D=>nx88, RN=>
      nx773);
   ix89 : OAI221 port map ( Q=>nx88, A=>nx793, B=>nx795, C=>athome, D=>nx790
   );
   ix794 : CLKIN1 port map ( Q=>nx793, A=>success);
   reg_state_6 : DFC1 port map ( Q=>OPEN, QN=>nx795, C=>clk, D=>nx76, RN=>
      nx773);
   ix77 : OAI221 port map ( Q=>nx76, A=>lostfood, B=>nx798, C=>success, D=>
      nx795);
   ix799 : NAND21 port map ( Q=>nx798, A=>closetofood, B=>state_5);
   reg_state_8 : DFC1 port map ( Q=>state_8, QN=>nx800, C=>clk, D=>nx100, RN
      =>nx773);
   reg_state_0 : DFP1 port map ( Q=>state_0, QN=>OPEN, C=>clk, D=>nx720, SN
      =>nx773);
   ix67 : NOR21 port map ( Q=>food, A=>nx764, B=>nx779);
   ix159 : NAND21 port map ( Q=>rest, A=>nx784, B=>nx782);
end Bot ;

