
-- 
-- Definition of  System
-- 
--      Tue May 19 10:11:32 2020
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity System is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      scantimeup : IN std_logic ;
      food : OUT std_logic) ;
end System ;

architecture OneHot of System is
   signal nx69, R_state_5, C2_count_3, C2_state, R_state_1, R_state_0, 
      R_state_8, nx18, nx30, nx42, R_state_3, nx79, nx58, R_state_2, nx81, 
      R_state_4, nx70, nx74, nx88, nx98, nx114, nx120, C1_count_3, C1_state, 
      nx83, nx130, nx140, C1_count_2, C1_count_1, C1_count_0, nx156, nx172, 
      nx87, nx190, nx202, nx218, nx234, nx244, C2_count_2, C2_count_1, 
      C2_count_0, nx260, nx276, nx90, nx294, nx306, nx332, nx99, nx103, 
      nx105, nx107, nx113, nx118, nx121, nx123, nx127, nx129, nx133, nx135, 
      nx138, nx143, nx147, nx149, nx151, nx155, nx160, nx169, nx175, nx177, 
      nx179, nx183, nx189, nx193, nx195, nx199, nx205, nx207, nx211, nx217, 
      nx219, nx224, nx229, nx231, nx237, nx239, nx242, nx245, nx249, nx253, 
      nx255, nx259, nx261, nx263, nx265, nx269, nx275, nx277, nx279, nx282: 
   std_logic ;

begin
   ix70 : TIE0 port map ( Q=>nx69);
   ix339 : CLKIN1 port map ( Q=>food, A=>nx99);
   ix100 : NAND21 port map ( Q=>nx99, A=>closetofood, B=>R_state_5);
   ix333 : OAI311 port map ( Q=>nx332, A=>lostfood, B=>nx103, C=>nx107, D=>
      nx282);
   R_reg_state_5 : DFC1 port map ( Q=>R_state_5, QN=>nx103, C=>clk, D=>nx332, 
      RN=>nx105);
   ix106 : INV3 port map ( Q=>nx105, A=>reset);
   ix108 : NAND21 port map ( Q=>nx107, A=>nx79, B=>nx99);
   ix321 : OAI211 port map ( Q=>nx79, A=>C2_count_2, B=>C2_count_1, C=>
      C2_count_3);
   ix295 : OAI311 port map ( Q=>nx294, A=>nx113, B=>nx90, C=>nx263, D=>nx279
   );
   ix114 : AOI211 port map ( Q=>nx113, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   ix277 : OAI211 port map ( Q=>nx276, A=>nx118, B=>nx121, C=>nx277);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx118, C=>clk, D=>
      nx276, RN=>nx105);
   ix122 : OAI211 port map ( Q=>nx121, A=>nx123, B=>nx83, C=>nx253);
   ix219 : OAI2111 port map ( Q=>nx218, A=>nx127, B=>nx129, C=>nx275, D=>
      nx229);
   ix128 : CLKIN1 port map ( Q=>nx127, A=>athome);
   ix115 : OAI221 port map ( Q=>nx114, A=>nx79, B=>nx133, C=>athome, D=>
      nx129);
   ix134 : AOI211 port map ( Q=>nx133, A=>nx135, B=>nx98, C=>nx58);
   ix136 : CLKIN1 port map ( Q=>nx135, A=>findfood);
   ix99 : OAI211 port map ( Q=>nx98, A=>scantimeup, B=>nx138, C=>nx265);
   R_reg_state_4 : DFC1 port map ( Q=>R_state_4, QN=>nx138, C=>clk, D=>nx74, 
      RN=>nx105);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>nx143);
   ix144 : AOI211 port map ( Q=>nx143, A=>lostfood, B=>R_state_5, C=>nx70);
   ix71 : NOR40 port map ( Q=>nx70, A=>scantimeup, B=>findfood, C=>nx138, D
      =>nx147);
   ix148 : AOI211 port map ( Q=>nx147, A=>nx149, B=>nx118, C=>nx151);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx149, C=>clk, D=>
      nx294, RN=>nx105);
   ix307 : OAI221 port map ( Q=>nx306, A=>nx155, B=>nx263, C=>nx151, D=>
      nx121);
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx151, C=>clk, D=>
      nx306, RN=>nx105);
   ix301 : NOR21 port map ( Q=>nx90, A=>nx149, B=>nx160);
   ix161 : NAND21 port map ( Q=>nx160, A=>C2_count_1, B=>C2_count_0);
   ix261 : OAI211 port map ( Q=>nx260, A=>C2_state, B=>nx255, C=>nx259);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx253, C=>clk, D=>nx234, 
      RN=>nx105);
   ix170 : OAI211 port map ( Q=>nx169, A=>C1_count_3, B=>C1_count_2, C=>
      R_state_1);
   ix203 : OAI221 port map ( Q=>nx202, A=>nx175, B=>nx177, C=>nx249, D=>
      nx242);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx175, C=>clk, D=>
      nx202, RN=>nx105);
   ix178 : NAND21 port map ( Q=>nx177, A=>nx179, B=>nx242);
   ix184 : AOI2111 port map ( Q=>nx183, A=>success, B=>R_state_8, C=>
      R_state_0, D=>R_state_3);
   ix43 : OAI221 port map ( Q=>nx42, A=>nx127, B=>nx189, C=>success, D=>
      nx199);
   R_reg_state_7 : DFC1 port map ( Q=>OPEN, QN=>nx189, C=>clk, D=>nx30, RN=>
      nx105);
   ix31 : OAI221 port map ( Q=>nx30, A=>nx193, B=>nx195, C=>athome, D=>nx189
   );
   ix194 : CLKIN1 port map ( Q=>nx193, A=>success);
   R_reg_state_6 : DFC1 port map ( Q=>OPEN, QN=>nx195, C=>clk, D=>nx18, RN=>
      nx105);
   ix19 : OAI221 port map ( Q=>nx18, A=>lostfood, B=>nx99, C=>success, D=>
      nx195);
   R_reg_state_8 : DFC1 port map ( Q=>R_state_8, QN=>nx199, C=>clk, D=>nx42, 
      RN=>nx105);
   R_reg_state_0 : DFP1 port map ( Q=>R_state_0, QN=>OPEN, C=>clk, D=>nx69, 
      SN=>nx105);
   R_reg_state_3 : DFC1 port map ( Q=>R_state_3, QN=>nx129, C=>clk, D=>nx114, 
      RN=>nx105);
   ix206 : NAND21 port map ( Q=>nx205, A=>nx175, B=>nx207);
   ix191 : OAI311 port map ( Q=>nx190, A=>nx211, B=>nx87, C=>nx242, D=>nx245
   );
   ix212 : AOI211 port map ( Q=>nx211, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix173 : OAI211 port map ( Q=>nx172, A=>nx217, B=>nx177, C=>nx219);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx217, C=>clk, D=>
      nx172, RN=>nx105);
   ix220 : OAI2111 port map ( Q=>nx219, A=>C1_count_0, B=>C1_count_1, C=>
      nx239, D=>nx130);
   ix157 : OAI211 port map ( Q=>nx156, A=>C1_state, B=>nx224, C=>nx231);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx179, C=>clk, D=>nx130, 
      RN=>nx105);
   ix121 : NAND21 port map ( Q=>nx120, A=>nx229, B=>nx129);
   ix230 : AOI211 port map ( Q=>nx229, A=>success, B=>R_state_8, C=>
      R_state_0);
   ix232 : NAND31 port map ( Q=>nx231, A=>nx83, B=>nx237, C=>C1_state);
   ix213 : NOR21 port map ( Q=>nx83, A=>C1_count_3, B=>C1_count_2);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx207, C=>clk, D=>
      nx190, RN=>nx105);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx237, C=>clk, D=>
      nx156, RN=>nx105);
   ix240 : NAND21 port map ( Q=>nx239, A=>C1_count_1, B=>C1_count_0);
   ix197 : NOR21 port map ( Q=>nx87, A=>nx207, B=>nx239);
   ix246 : NAND21 port map ( Q=>nx245, A=>C1_count_2, B=>nx140);
   ix141 : NOR21 port map ( Q=>nx140, A=>C1_state, B=>nx120);
   R_reg_state_1 : DFC1 port map ( Q=>R_state_1, QN=>nx123, C=>clk, D=>nx218, 
      RN=>nx105);
   ix227 : NOR21 port map ( Q=>nx81, A=>nx123, B=>nx83);
   ix260 : NAND31 port map ( Q=>nx259, A=>nx79, B=>nx261, C=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx261, C=>clk, D=>
      nx260, RN=>nx105);
   ix89 : OAI221 port map ( Q=>nx88, A=>findfood, B=>nx269, C=>nx123, D=>
      nx83);
   ix270 : AOI221 port map ( Q=>nx269, A=>scantimeup, B=>R_state_4, C=>
      R_state_2, D=>nx79);
   R_reg_state_2 : DFC1 port map ( Q=>R_state_2, QN=>nx265, C=>clk, D=>nx88, 
      RN=>nx105);
   ix59 : NOR31 port map ( Q=>nx58, A=>closetofood, B=>nx103, C=>lostfood);
   ix276 : NAND21 port map ( Q=>nx275, A=>R_state_1, B=>nx83);
   ix278 : OAI2111 port map ( Q=>nx277, A=>C2_count_0, B=>C2_count_1, C=>
      nx160, D=>nx234);
   ix280 : NAND21 port map ( Q=>nx279, A=>C2_count_2, B=>nx244);
   ix245 : NOR21 port map ( Q=>nx244, A=>C2_state, B=>nx81);
   ix284 : OAI211 port map ( Q=>nx282, A=>R_state_2, B=>R_state_4, C=>
      findfood);
   ix156 : XOR21 port map ( Q=>nx155, A=>nx151, B=>nx90);
   ix235 : IMUX21 port map ( Q=>nx234, A=>nx147, B=>nx169, S=>nx253);
   ix131 : IMUX21 port map ( Q=>nx130, A=>nx205, B=>nx183, S=>nx179);
   ix226 : XOR21 port map ( Q=>nx224, A=>nx237, B=>nx120);
   ix244 : IMUX21 port map ( Q=>nx242, A=>nx83, B=>nx120, S=>nx179);
   ix250 : XOR21 port map ( Q=>nx249, A=>nx175, B=>nx87);
   ix256 : XOR21 port map ( Q=>nx255, A=>nx261, B=>nx81);
   ix264 : IMUX21 port map ( Q=>nx263, A=>nx79, B=>nx81, S=>nx253);
end OneHot ;

