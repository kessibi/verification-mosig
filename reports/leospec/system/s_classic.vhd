
-- 
-- Definition of  System
-- 
--      Tue May 19 10:10:16 2020
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity LIB_ROBOT_Robot_classic is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end LIB_ROBOT_Robot_classic ;

architecture Classic of LIB_ROBOT_Robot_classic is
   signal rest_EXMPLR, food_EXMPLR, state_2, state_0, state_1, nx465, nx14, 
      nx466, nx24, nx38, nx60, nx74, state_3, nx100, nx116, nx138, nx148, 
      nx164, nx170, nx180, nx186, nx474, nx477, nx480, nx482, nx485, nx491, 
      nx494, nx496, nx498, nx503, nx505, nx507, nx509, nx512, nx514, nx516, 
      nx519, nx522, nx525, nx527, nx529, nx532, nx538, nx540, nx542, nx545: 
   std_logic ;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix203 : NOR40 port map ( Q=>food_EXMPLR, A=>nx474, B=>state_1, C=>nx542, 
      D=>nx505);
   ix191 : OAI2111 port map ( Q=>nx465, A=>nx477, B=>nx532, C=>nx485, D=>
      nx540);
   ix478 : NAND21 port map ( Q=>nx477, A=>state_2, B=>nx482);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx474, C=>clk, D=>nx465, RN
      =>nx480);
   ix481 : CLKIN1 port map ( Q=>nx480, A=>reset);
   ix39 : OAI2111 port map ( Q=>nx38, A=>lostfood, B=>nx485, C=>nx522, D=>
      nx527);
   ix486 : NAND31 port map ( Q=>nx485, A=>nx466, B=>closetofood, C=>state_0
   );
   ix199 : NOR21 port map ( Q=>nx466, A=>nx474, B=>state_1);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx482, C=>clk, D=>nx38, RN
      =>nx480);
   ix149 : NAND41 port map ( Q=>nx148, A=>nx491, B=>nx498, C=>nx519, D=>
      nx525);
   ix492 : OAI211 port map ( Q=>nx491, A=>nx138, B=>nx474, C=>nx465);
   ix139 : OAI311 port map ( Q=>nx138, A=>state_0, B=>nx494, C=>state_1, D=>
      nx496);
   ix495 : CLKIN1 port map ( Q=>nx494, A=>findfood);
   ix497 : OAI211 port map ( Q=>nx496, A=>state_0, B=>success, C=>state_1);
   ix499 : AOI311 port map ( Q=>nx498, A=>state_0, B=>nx474, C=>state_1, D=>
      nx116);
   ix117 : AOI2111 port map ( Q=>nx116, A=>state_3, B=>nx516, C=>state_0, D
      =>nx24);
   ix101 : AOI211 port map ( Q=>nx100, A=>nx503, B=>nx509, C=>rest_EXMPLR);
   ix504 : NAND21 port map ( Q=>nx503, A=>nx505, B=>nx507);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx505, C=>clk, D=>nx148, RN
      =>nx480);
   ix508 : NOR21 port map ( Q=>nx507, A=>state_1, B=>state_2);
   ix510 : NAND31 port map ( Q=>nx509, A=>state_0, B=>athome, C=>state_1);
   ix125 : OAI311 port map ( Q=>rest_EXMPLR, A=>nx505, B=>state_2, C=>nx482, 
      D=>nx512);
   ix513 : OAI2111 port map ( Q=>nx512, A=>nx514, B=>success, C=>nx505, D=>
      nx507);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx514, C=>clk, D=>nx100, RN
      =>nx480);
   ix517 : CLKIN1 port map ( Q=>nx516, A=>success);
   ix25 : NAND21 port map ( Q=>nx24, A=>nx482, B=>nx474);
   ix520 : AOI311 port map ( Q=>nx519, A=>nx74, B=>state_0, C=>nx522, D=>
      nx60);
   ix75 : OAI311 port map ( Q=>nx74, A=>food_EXMPLR, B=>lostfood, C=>state_1, 
      D=>state_2);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx507, B=>aboverestth, C=>state_0
   );
   ix61 : NOR40 port map ( Q=>nx60, A=>scantimeup, B=>nx474, C=>state_0, D=>
      nx465);
   ix526 : NAND31 port map ( Q=>nx525, A=>state_1, B=>abovesearchth, C=>
      nx474);
   ix528 : AOI221 port map ( Q=>nx527, A=>nx529, B=>nx466, C=>state_1, D=>
      nx14);
   ix530 : NOR40 port map ( Q=>nx529, A=>nx186, B=>food_EXMPLR, C=>nx170, D
      =>nx164);
   ix187 : NOR21 port map ( Q=>nx186, A=>nx477, B=>nx532);
   ix533 : AOI211 port map ( Q=>nx532, A=>lostfood, B=>state_0, C=>nx180);
   ix181 : AOI211 port map ( Q=>nx180, A=>nx505, B=>scantimeup, C=>
      abovesearchth);
   ix171 : NOR31 port map ( Q=>nx170, A=>nx507, B=>nx494, C=>state_0);
   ix165 : AOI2111 port map ( Q=>nx164, A=>state_0, B=>athome, C=>nx474, D=>
      nx482);
   ix15 : OAI211 port map ( Q=>nx14, A=>athome, B=>nx505, C=>nx538);
   ix541 : AOI311 port map ( Q=>nx540, A=>nx24, B=>findfood, C=>nx505, D=>
      nx164);
   ix543 : CLKIN1 port map ( Q=>nx542, A=>closetofood);
   ix31 : NOR40 port map ( Q=>search, A=>state_1, B=>state_2, C=>nx545, D=>
      nx505);
   ix546 : CLKIN1 port map ( Q=>nx545, A=>aboverestth);
   ix539 : MUX22 port map ( Q=>nx538, A=>state_0, B=>nx474, S=>nx465);
end Classic ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity System is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      scantimeup : IN std_logic ;
      food : OUT std_logic) ;
end System ;

architecture Classic of System is
   component LIB_ROBOT_Robot_classic
      port (
         reset : IN std_logic ;
         clk : IN std_logic ;
         athome : IN std_logic ;
         findfood : IN std_logic ;
         lostfood : IN std_logic ;
         closetofood : IN std_logic ;
         success : IN std_logic ;
         aboverestth : IN std_logic ;
         abovesearchth : IN std_logic ;
         scantimeup : IN std_logic ;
         rest : OUT std_logic ;
         search : OUT std_logic ;
         food : OUT std_logic) ;
   end component ;
   signal s_aboverestth, s_abovesearchth, s_rest, s_search, C2_count_3, 
      C2_state, nx8, nx18, C2_count_2, C2_count_1, C2_count_0, nx36, nx52, 
      nx54, nx70, nx82, C1_count_3, C1_state, nx100, nx110, C1_count_2, 
      C1_count_1, C1_count_0, nx128, nx144, nx56, nx162, nx174, nx67, nx74, 
      nx76, nx79, nx81, nx86, nx89, nx91, nx95, nx97, nx101, nx105, nx107, 
      nx113, nx115, nx117, nx119, nx125, nx129, nx133, nx139, nx141, nx143, 
      nx148, nx150, nx153, nx155, nx161, nx163, nx167, nx169, nx171, nx173: 
   std_logic ;

begin
   R : LIB_ROBOT_Robot_classic port map ( reset=>reset, clk=>clk, athome=>athome, 
      findfood=>findfood, lostfood=>lostfood, closetofood=>closetofood, 
      success=>success, aboverestth=>s_aboverestth, abovesearchth=>
      s_abovesearchth, scantimeup=>scantimeup, rest=>s_rest, search=>
      s_search, food=>food);
   ix91 : AOI211 port map ( Q=>s_abovesearchth, A=>nx67, B=>nx97, C=>nx115);
   ix53 : OAI311 port map ( Q=>nx52, A=>nx67, B=>C2_state, C=>s_search, D=>
      nx81);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx79, C=>clk, D=>nx8, RN
      =>nx76);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>s_search);
   ix78 : CLKIN1 port map ( Q=>nx76, A=>reset);
   ix82 : OAI2111 port map ( Q=>nx81, A=>C2_count_0, B=>C2_count_1, C=>nx95, 
      D=>nx8);
   ix37 : OAI221 port map ( Q=>nx36, A=>C2_state, B=>nx86, C=>
      s_abovesearchth, D=>nx89);
   ix90 : NAND21 port map ( Q=>nx89, A=>nx91, B=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx91, C=>clk, D=>nx36, 
      RN=>nx76);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx67, C=>clk, D=>nx52, 
      RN=>nx76);
   ix96 : NAND21 port map ( Q=>nx95, A=>C2_count_1, B=>C2_count_0);
   ix71 : OAI311 port map ( Q=>nx70, A=>nx101, B=>nx54, C=>nx105, D=>nx119);
   ix102 : AOI211 port map ( Q=>nx101, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx97, C=>clk, D=>nx70, 
      RN=>nx76);
   ix77 : NOR21 port map ( Q=>nx54, A=>nx97, B=>nx95);
   ix108 : OAI211 port map ( Q=>nx107, A=>C2_count_1, B=>C2_count_2, C=>
      C2_count_3);
   ix83 : OAI221 port map ( Q=>nx82, A=>nx113, B=>nx105, C=>nx115, D=>nx117
   );
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx115, C=>clk, D=>
      nx82, RN=>nx76);
   ix118 : NAND21 port map ( Q=>nx117, A=>nx79, B=>nx74);
   ix120 : NAND21 port map ( Q=>nx119, A=>C2_count_2, B=>nx18);
   ix19 : NOR21 port map ( Q=>nx18, A=>C2_state, B=>s_search);
   ix181 : NAND21 port map ( Q=>s_aboverestth, A=>nx125, B=>nx169);
   ix163 : OAI311 port map ( Q=>nx162, A=>nx129, B=>nx56, C=>nx161, D=>nx173
   );
   ix130 : AOI211 port map ( Q=>nx129, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix145 : OAI311 port map ( Q=>nx144, A=>nx133, B=>C1_state, C=>s_rest, D=>
      nx143);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx133, C=>clk, D=>
      nx144, RN=>nx76);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx141, C=>clk, D=>nx100, 
      RN=>nx76);
   ix140 : CLKIN1 port map ( Q=>nx139, A=>s_rest);
   ix144 : OAI2111 port map ( Q=>nx143, A=>C1_count_0, B=>C1_count_1, C=>
      nx155, D=>nx100);
   ix129 : OAI221 port map ( Q=>nx128, A=>C1_state, B=>nx148, C=>
      s_aboverestth, D=>nx150);
   ix152 : NAND21 port map ( Q=>nx150, A=>nx153, B=>C1_state);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx153, C=>clk, D=>
      nx128, RN=>nx76);
   ix156 : NAND21 port map ( Q=>nx155, A=>C1_count_1, B=>C1_count_0);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx125, C=>clk, D=>
      nx162, RN=>nx76);
   ix169 : NOR21 port map ( Q=>nx56, A=>nx125, B=>nx155);
   ix164 : NOR21 port map ( Q=>nx163, A=>C1_count_2, B=>C1_count_3);
   ix175 : OAI221 port map ( Q=>nx174, A=>nx167, B=>nx161, C=>nx169, D=>
      nx171);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx169, C=>clk, D=>
      nx174, RN=>nx76);
   ix172 : NAND21 port map ( Q=>nx171, A=>nx141, B=>nx139);
   ix174 : NAND21 port map ( Q=>nx173, A=>C1_count_2, B=>nx110);
   ix111 : NOR21 port map ( Q=>nx110, A=>C1_state, B=>s_rest);
   ix9 : IMUX21 port map ( Q=>nx8, A=>s_abovesearchth, B=>nx74, S=>nx79);
   ix87 : XOR21 port map ( Q=>nx86, A=>nx91, B=>s_search);
   ix106 : IMUX21 port map ( Q=>nx105, A=>nx107, B=>s_search, S=>nx79);
   ix114 : XOR21 port map ( Q=>nx113, A=>nx115, B=>nx54);
   ix101 : IMUX21 port map ( Q=>nx100, A=>s_aboverestth, B=>nx139, S=>nx141
   );
   ix149 : XOR21 port map ( Q=>nx148, A=>nx153, B=>s_rest);
   ix162 : IMUX21 port map ( Q=>nx161, A=>nx163, B=>s_rest, S=>nx141);
   ix168 : XOR21 port map ( Q=>nx167, A=>nx169, B=>nx56);
end Classic ;

