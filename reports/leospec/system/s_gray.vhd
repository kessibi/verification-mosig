
-- 
-- Definition of  System
-- 
--      Tue May 19 10:10:45 2020
--      
--      LeonardoSpectrum Level 3, 2015a.6
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity LIB_ROBOT_Robot_gray is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end LIB_ROBOT_Robot_gray ;

architecture Gray of LIB_ROBOT_Robot_gray is
   signal rest_EXMPLR, food_EXMPLR, state_1, state_0, nx488, nx10, state_3, 
      nx26, state_2, nx40, nx50, nx490, nx56, nx76, nx82, nx92, nx104, nx118, 
      nx120, nx140, nx148, nx174, nx498, nx501, nx504, nx507, nx510, nx514, 
      nx516, nx519, nx522, nx526, nx529, nx532, nx534, nx537, nx540, nx544, 
      nx546, nx550, nx553, nx555, nx557, nx559, nx562, nx565, nx567, nx570: 
   std_logic ;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix185 : CLKIN1 port map ( Q=>food_EXMPLR, A=>nx498);
   ix499 : NAND31 port map ( Q=>nx498, A=>nx488, B=>closetofood, C=>state_0
   );
   ix181 : NOR21 port map ( Q=>nx488, A=>nx501, B=>nx544);
   ix175 : OAI2111 port map ( Q=>nx174, A=>food_EXMPLR, B=>nx504, C=>nx565, 
      D=>nx567);
   ix505 : OAI2111 port map ( Q=>nx504, A=>nx26, B=>state_2, C=>nx519, D=>
      state_1);
   ix27 : NAND21 port map ( Q=>nx26, A=>nx507, B=>athome);
   ix149 : OAI2111 port map ( Q=>nx148, A=>lostfood, B=>nx510, C=>nx516, D=>
      nx559);
   ix511 : AOI211 port map ( Q=>nx510, A=>nx488, B=>nx10, C=>food_EXMPLR);
   ix11 : OAI211 port map ( Q=>nx10, A=>abovesearchth, B=>nx507, C=>state_1
   );
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx501, C=>clk, D=>nx174, RN
      =>nx514);
   ix515 : CLKIN1 port map ( Q=>nx514, A=>reset);
   ix517 : AOI221 port map ( Q=>nx516, A=>success, B=>nx490, C=>nx40, D=>
      nx140);
   ix99 : NOR21 port map ( Q=>nx490, A=>nx519, B=>nx534);
   ix105 : OAI221 port map ( Q=>nx104, A=>nx26, B=>nx522, C=>success, D=>
      nx526);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx501, B=>nx519, C=>state_2);
   ix93 : OAI2111 port map ( Q=>nx92, A=>success, B=>nx526, C=>nx498, D=>
      nx529);
   ix527 : NAND21 port map ( Q=>nx526, A=>state_3, B=>state_2);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx519, C=>clk, D=>nx104, RN
      =>nx514);
   ix530 : AOI311 port map ( Q=>nx529, A=>nx50, B=>findfood, C=>nx537, D=>
      nx82);
   ix51 : OAI221 port map ( Q=>nx50, A=>nx501, B=>nx532, C=>state_3, D=>
      state_0);
   ix533 : NAND21 port map ( Q=>nx532, A=>nx519, B=>nx534);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx534, C=>clk, D=>nx92, RN
      =>nx514);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx507, C=>clk, D=>nx148, RN
      =>nx514);
   ix538 : OAI211 port map ( Q=>nx537, A=>nx56, B=>nx40, C=>nx507);
   ix57 : NOR40 port map ( Q=>nx56, A=>nx519, B=>nx534, C=>nx540, D=>state_1
   );
   ix541 : CLKIN1 port map ( Q=>nx540, A=>success);
   ix41 : NOR21 port map ( Q=>nx40, A=>state_3, B=>state_2);
   ix83 : NOR21 port map ( Q=>nx82, A=>nx544, B=>nx546);
   ix545 : NAND21 port map ( Q=>nx544, A=>nx519, B=>state_2);
   ix547 : AOI2111 port map ( Q=>nx546, A=>lostfood, B=>state_0, C=>nx76, D
      =>nx10);
   ix77 : NOR21 port map ( Q=>nx76, A=>abovesearchth, B=>scantimeup);
   ix141 : OAI2111 port map ( Q=>nx140, A=>nx550, B=>rest_EXMPLR, C=>nx555, 
      D=>nx26);
   ix551 : CLKIN1 port map ( Q=>nx550, A=>findfood);
   ix63 : NOR21 port map ( Q=>rest_EXMPLR, A=>state_0, B=>nx553);
   ix554 : AOI311 port map ( Q=>nx553, A=>nx490, B=>success, C=>nx501, D=>
      nx40);
   ix556 : AOI211 port map ( Q=>nx555, A=>nx557, B=>state_0, C=>nx501);
   ix558 : CLKIN1 port map ( Q=>nx557, A=>abovesearchth);
   ix560 : AOI311 port map ( Q=>nx559, A=>nx120, B=>nx507, C=>nx488, D=>
      nx118);
   ix121 : NAND21 port map ( Q=>nx120, A=>nx562, B=>nx550);
   ix563 : CLKIN1 port map ( Q=>nx562, A=>scantimeup);
   ix119 : NOR40 port map ( Q=>nx118, A=>success, B=>nx507, C=>nx544, D=>
      state_1);
   ix566 : NAND21 port map ( Q=>nx565, A=>lostfood, B=>food_EXMPLR);
   ix568 : NAND41 port map ( Q=>nx567, A=>aboverestth, B=>nx501, C=>state_0, 
      D=>nx40);
   ix157 : NOR40 port map ( Q=>search, A=>nx570, B=>nx507, C=>state_3, D=>
      state_2);
   ix571 : NAND21 port map ( Q=>nx570, A=>aboverestth, B=>nx501);
end Gray ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity System is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      scantimeup : IN std_logic ;
      food : OUT std_logic) ;
end System ;

architecture Gray of System is
   component LIB_ROBOT_Robot_gray
      port (
         reset : IN std_logic ;
         clk : IN std_logic ;
         athome : IN std_logic ;
         findfood : IN std_logic ;
         lostfood : IN std_logic ;
         closetofood : IN std_logic ;
         success : IN std_logic ;
         aboverestth : IN std_logic ;
         abovesearchth : IN std_logic ;
         scantimeup : IN std_logic ;
         rest : OUT std_logic ;
         search : OUT std_logic ;
         food : OUT std_logic) ;
   end component ;
   signal s_aboverestth, s_abovesearchth, s_rest, s_search, C2_count_3, 
      C2_state, nx8, nx18, C2_count_2, C2_count_1, C2_count_0, nx36, nx52, 
      nx54, nx70, nx82, C1_count_3, C1_state, nx100, nx110, C1_count_2, 
      C1_count_1, C1_count_0, nx128, nx144, nx56, nx162, nx174, nx67, nx74, 
      nx76, nx79, nx81, nx86, nx89, nx91, nx95, nx97, nx101, nx105, nx107, 
      nx113, nx115, nx117, nx119, nx125, nx129, nx133, nx139, nx141, nx143, 
      nx148, nx150, nx153, nx155, nx161, nx163, nx167, nx169, nx171, nx173: 
   std_logic ;

begin
   R : LIB_ROBOT_Robot_gray port map ( reset=>reset, clk=>clk, athome=>athome, 
      findfood=>findfood, lostfood=>lostfood, closetofood=>closetofood, 
      success=>success, aboverestth=>s_aboverestth, abovesearchth=>
      s_abovesearchth, scantimeup=>scantimeup, rest=>s_rest, search=>
      s_search, food=>food);
   ix91 : AOI211 port map ( Q=>s_abovesearchth, A=>nx67, B=>nx97, C=>nx115);
   ix53 : OAI311 port map ( Q=>nx52, A=>nx67, B=>C2_state, C=>s_search, D=>
      nx81);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx79, C=>clk, D=>nx8, RN
      =>nx76);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>s_search);
   ix78 : CLKIN1 port map ( Q=>nx76, A=>reset);
   ix82 : OAI2111 port map ( Q=>nx81, A=>C2_count_0, B=>C2_count_1, C=>nx95, 
      D=>nx8);
   ix37 : OAI221 port map ( Q=>nx36, A=>C2_state, B=>nx86, C=>
      s_abovesearchth, D=>nx89);
   ix90 : NAND21 port map ( Q=>nx89, A=>nx91, B=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx91, C=>clk, D=>nx36, 
      RN=>nx76);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx67, C=>clk, D=>nx52, 
      RN=>nx76);
   ix96 : NAND21 port map ( Q=>nx95, A=>C2_count_1, B=>C2_count_0);
   ix71 : OAI311 port map ( Q=>nx70, A=>nx101, B=>nx54, C=>nx105, D=>nx119);
   ix102 : AOI211 port map ( Q=>nx101, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx97, C=>clk, D=>nx70, 
      RN=>nx76);
   ix77 : NOR21 port map ( Q=>nx54, A=>nx97, B=>nx95);
   ix108 : OAI211 port map ( Q=>nx107, A=>C2_count_1, B=>C2_count_2, C=>
      C2_count_3);
   ix83 : OAI221 port map ( Q=>nx82, A=>nx113, B=>nx105, C=>nx115, D=>nx117
   );
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx115, C=>clk, D=>
      nx82, RN=>nx76);
   ix118 : NAND21 port map ( Q=>nx117, A=>nx79, B=>nx74);
   ix120 : NAND21 port map ( Q=>nx119, A=>C2_count_2, B=>nx18);
   ix19 : NOR21 port map ( Q=>nx18, A=>C2_state, B=>s_search);
   ix181 : NAND21 port map ( Q=>s_aboverestth, A=>nx125, B=>nx169);
   ix163 : OAI311 port map ( Q=>nx162, A=>nx129, B=>nx56, C=>nx161, D=>nx173
   );
   ix130 : AOI211 port map ( Q=>nx129, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix145 : OAI311 port map ( Q=>nx144, A=>nx133, B=>C1_state, C=>s_rest, D=>
      nx143);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx133, C=>clk, D=>
      nx144, RN=>nx76);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx141, C=>clk, D=>nx100, 
      RN=>nx76);
   ix140 : CLKIN1 port map ( Q=>nx139, A=>s_rest);
   ix144 : OAI2111 port map ( Q=>nx143, A=>C1_count_0, B=>C1_count_1, C=>
      nx155, D=>nx100);
   ix129 : OAI221 port map ( Q=>nx128, A=>C1_state, B=>nx148, C=>
      s_aboverestth, D=>nx150);
   ix152 : NAND21 port map ( Q=>nx150, A=>nx153, B=>C1_state);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx153, C=>clk, D=>
      nx128, RN=>nx76);
   ix156 : NAND21 port map ( Q=>nx155, A=>C1_count_1, B=>C1_count_0);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx125, C=>clk, D=>
      nx162, RN=>nx76);
   ix169 : NOR21 port map ( Q=>nx56, A=>nx125, B=>nx155);
   ix164 : NOR21 port map ( Q=>nx163, A=>C1_count_2, B=>C1_count_3);
   ix175 : OAI221 port map ( Q=>nx174, A=>nx167, B=>nx161, C=>nx169, D=>
      nx171);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx169, C=>clk, D=>
      nx174, RN=>nx76);
   ix172 : NAND21 port map ( Q=>nx171, A=>nx141, B=>nx139);
   ix174 : NAND21 port map ( Q=>nx173, A=>C1_count_2, B=>nx110);
   ix111 : NOR21 port map ( Q=>nx110, A=>C1_state, B=>s_rest);
   ix9 : IMUX21 port map ( Q=>nx8, A=>s_abovesearchth, B=>nx74, S=>nx79);
   ix87 : XOR21 port map ( Q=>nx86, A=>nx91, B=>s_search);
   ix106 : IMUX21 port map ( Q=>nx105, A=>nx107, B=>s_search, S=>nx79);
   ix114 : XOR21 port map ( Q=>nx113, A=>nx115, B=>nx54);
   ix101 : IMUX21 port map ( Q=>nx100, A=>s_aboverestth, B=>nx139, S=>nx141
   );
   ix149 : XOR21 port map ( Q=>nx148, A=>nx153, B=>s_rest);
   ix162 : IMUX21 port map ( Q=>nx161, A=>nx163, B=>s_rest, S=>nx141);
   ix168 : XOR21 port map ( Q=>nx167, A=>nx169, B=>nx56);
end Gray ;

