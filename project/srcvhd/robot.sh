#!/bin/bash

# source ../confmodelsim/config 

## nettoyage
if [ -d ../libs/LIB_ROBOT ]
then /bin/rm -rf ../libs/LIB_ROBOT
fi

## creation de la librairie de travail
vlib ../libs/LIB_ROBOT

## compilation
vcom -work LIB_ROBOT +cover=fsb robot.vhd
