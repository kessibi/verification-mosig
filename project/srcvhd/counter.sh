#!/bin/bash

# source ../confmodelsim/config 

## nettoyage
if [ -d ../libs/LIB_COUNTER ]
then /bin/rm -rf ../libs/LIB_COUNTER
fi

## creation de la librairie de travail
vlib ../libs/LIB_COUNTER

## compilation
vcom -work LIB_COUNTER counter.vhd
