library IEEE;
use IEEE.std_logic_1164.all;

library c35_CORELIB;
use c35_CORELIB.vcomponents.all;

entity Robot is
  port(reset, clk, athome, findfood, lostfood, closetofood, success, aboverestth, abovesearchth, scantimeup : in std_logic;
  rest, search, food : out std_logic);
end Robot;

architecture Orig of Robot is
  type States is (Idle, Resting, Randomwalk, Homing, Scanarena, Movetofood, Grabfood, Movetohome, Deposit);
  signal state, nextstate : States;

-- PSL default clock is (clk'event and clk = '1');

-- PSL property P1 is
-- always (
--   state = Randomwalk
--   -> (not (state = Grabfood)) until! findfood = '1'
-- );
-- PSL assert P1;

-- PSL property P2 is
-- always (
--   state = Randomwalk
--   -> (not (state = Homing)) until! abovesearchth = '1'
-- );
-- PSL assert P2;

-- PSL property P3 is
-- always (
--   state = Grabfood
--   -> (eventually! state = Deposit) and (eventually! state = Resting)
-- );
-- PSL assert P3;

-- PSL property P4 is
-- always (
--   { (state = Randomwalk)
--   ; (abovesearchth = '0' and findfood = '0')[*]
--   ; (abovesearchth = '0' and findfood = '1')
--   ; (abovesearchth = '0' and lostfood = '0')[*]
--   ; (abovesearchth = '0' and lostfood = '1')
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
--   }
--   |-> next! (state = Randomwalk)
-- );
-- PSL assert P4;

-- PSL property P5 is
-- always (
--   state = Resting -> eventually! (state = Randomwalk)
-- );
-- PSL assert P5;

begin
  -- next state
  process(state, athome, findfood, lostfood, closetofood, success, aboverestth, abovesearchth, scantimeup)
  begin
    case state is
      when Idle =>
        nextstate <= Resting;

      when Resting =>
        if (aboverestth = '1') then
          nextstate <= Randomwalk;
        else
          nextstate <= state;
        end if;

      when Randomwalk =>
        if (findfood = '1') then
          nextstate <= Movetofood;
        elsif (abovesearchth = '1') then
          nextstate <= Homing;
        else
          nextstate <= state;
        end if;

      when Movetofood =>
        if (lostfood = '1') then
          nextstate <= Scanarena;
        elsif (closetofood = '1') then
          nextstate <= Grabfood;
        elsif (abovesearchth = '1') then
          nextstate <= Homing;
        else
          nextstate <= state;
        end if;

      when Scanarena =>
        if (findfood = '1') then
          nextstate <= Movetofood;
        elsif (scantimeup = '1') then
          nextstate <= Randomwalk;
        elsif (abovesearchth = '1') then
          nextstate <= Homing;
        else
          nextstate <= state;
        end if;

      when Homing =>
        if (athome = '1') then
          nextstate <= Resting;
        else
          nextstate <= state;
        end if;

      when Grabfood =>
        if (success = '1') then
          nextstate <= Movetohome;
        else
          nextstate <= state;
        end if;

      when Movetohome =>
        if (athome = '1') then
          nextstate <= Deposit;
        else
          nextstate <= state;
        end if;

      when Deposit =>
        if (success = '1') then
          nextstate <= Resting;
        else
          nextstate <= state;
        end if;
    end case;
  end process;

  -- update state
  process (reset, clk)
  begin
    if (reset = '1') then
      state <= Idle;
    elsif (clk'event and clk = '1') then
      state <= nextstate;
    end if;
  end process;

  -- output function
  process (state, athome, findfood, lostfood, closetofood, success, aboverestth, abovesearchth, scantimeup)
  begin
    -- rest
    if (state = Idle) or (state = Deposit and success = '1') or (state = Homing) then
      rest <= '1';
    else
      rest <= '0';
    end if;

    -- search
    if (state = Resting and aboverestth = '1') then
      search <= '1';
    else
      search <= '0';
    end if;

    -- food
    if (state = Movetofood and closetofood = '1') then
      food <= '1';
    else
      food <= '0';
    end if;
  end process;
end Orig;

-- architecture Classic of Robot is
--    signal rest_EXMPLR, food_EXMPLR, state_2, state_0, state_1, nx465, nx14,
--       nx466, nx24, nx38, nx60, nx74, state_3, nx100, nx116, nx138, nx148,
--       nx164, nx170, nx180, nx186, nx474, nx477, nx480, nx482, nx485, nx491,
--       nx494, nx496, nx498, nx503, nx505, nx507, nx509, nx512, nx514, nx516,
--       nx519, nx522, nx525, nx527, nx529, nx532, nx538, nx540, nx542, nx545:
--    std_logic ;
--
-- -- PSL default clock is (clk'event and clk = '1');
--
-- -- PSL property P1 is
-- -- always (
-- --   (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- --   -> (not (state_2 = '1' and state_1 = '1' and state_0 = '0'))
-- --      until! findfood = '1'
-- -- );
-- -- PSL assert P1;
--
-- -- PSL property P2 is
-- -- always (
-- --   (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- --   -> (not (state_2 = '0' and state_1 = '1' and state_0 = '1'))
-- --      until! abovesearchth = '1'
-- -- );
-- -- PSL assert P2;
--
-- -- PSL property P3 is
-- -- always (
-- --   (state_2 = '1' and state_1 = '1' and state_0 = '0')
-- --   -> (eventually! (state_3 = '1' and state_2 = '0' and state_1 = '0' and state_0 = '0'))
-- --      and
-- --      (eventually! (state_2 = '0' and state_1 = '0' and state_0 = '1'))
-- -- );
-- -- PSL assert P3;
--
-- -- PSL property P4 is
-- -- always (
-- --   { (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- --   ; (abovesearchth = '0' and findfood = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '1')
-- --   ; (abovesearchth = '0' and lostfood = '0')[*]
-- --   ; (abovesearchth = '0' and lostfood = '1')
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
-- --   }
-- --   |-> next! (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- -- );
-- -- PSL assert P4;
--
-- -- PSL property P5 is
-- -- always (
-- --   (state_2 = '0' and state_1 = '0' and state_0 = '1')
-- --   -> eventually! (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- -- );
-- -- PSL assert P5;
--
-- begin
--    rest <= rest_EXMPLR ;
--    food <= food_EXMPLR ;
--    ix203 : NOR40 port map ( Q=>food_EXMPLR, A=>nx474, B=>state_1, C=>nx542,
--       D=>nx505);
--    ix191 : OAI2111 port map ( Q=>nx465, A=>nx477, B=>nx532, C=>nx485, D=>
--       nx540);
--    ix478 : NAND21 port map ( Q=>nx477, A=>state_2, B=>nx482);
--    reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx474, C=>clk, D=>nx465, RN
--       =>nx480);
--    ix481 : CLKIN1 port map ( Q=>nx480, A=>reset);
--    ix39 : OAI2111 port map ( Q=>nx38, A=>lostfood, B=>nx485, C=>nx522, D=>
--       nx527);
--    ix486 : NAND31 port map ( Q=>nx485, A=>nx466, B=>closetofood, C=>state_0
--    );
--    ix199 : NOR21 port map ( Q=>nx466, A=>nx474, B=>state_1);
--    reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx482, C=>clk, D=>nx38, RN
--       =>nx480);
--    ix149 : NAND41 port map ( Q=>nx148, A=>nx491, B=>nx498, C=>nx519, D=>
--       nx525);
--    ix492 : OAI211 port map ( Q=>nx491, A=>nx138, B=>nx474, C=>nx465);
--    ix139 : OAI311 port map ( Q=>nx138, A=>state_0, B=>nx494, C=>state_1, D=>
--       nx496);
--    ix495 : CLKIN1 port map ( Q=>nx494, A=>findfood);
--    ix497 : OAI211 port map ( Q=>nx496, A=>state_0, B=>success, C=>state_1);
--    ix499 : AOI311 port map ( Q=>nx498, A=>state_0, B=>nx474, C=>state_1, D=>
--       nx116);
--    ix117 : AOI2111 port map ( Q=>nx116, A=>state_3, B=>nx516, C=>state_0, D
--       =>nx24);
--    ix101 : AOI211 port map ( Q=>nx100, A=>nx503, B=>nx509, C=>rest_EXMPLR);
--    ix504 : NAND21 port map ( Q=>nx503, A=>nx505, B=>nx507);
--    reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx505, C=>clk, D=>nx148, RN
--       =>nx480);
--    ix508 : NOR21 port map ( Q=>nx507, A=>state_1, B=>state_2);
--    ix510 : NAND31 port map ( Q=>nx509, A=>state_0, B=>athome, C=>state_1);
--    ix125 : OAI311 port map ( Q=>rest_EXMPLR, A=>nx505, B=>state_2, C=>nx482,
--       D=>nx512);
--    ix513 : OAI2111 port map ( Q=>nx512, A=>nx514, B=>success, C=>nx505, D=>
--       nx507);
--    reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx514, C=>clk, D=>nx100, RN
--       =>nx480);
--    ix517 : CLKIN1 port map ( Q=>nx516, A=>success);
--    ix25 : NAND21 port map ( Q=>nx24, A=>nx482, B=>nx474);
--    ix520 : AOI311 port map ( Q=>nx519, A=>nx74, B=>state_0, C=>nx522, D=>
--       nx60);
--    ix75 : OAI311 port map ( Q=>nx74, A=>food_EXMPLR, B=>lostfood, C=>state_1,
--       D=>state_2);
--    ix523 : NAND31 port map ( Q=>nx522, A=>nx507, B=>aboverestth, C=>state_0
--    );
--    ix61 : NOR40 port map ( Q=>nx60, A=>scantimeup, B=>nx474, C=>state_0, D=>
--       nx465);
--    ix526 : NAND31 port map ( Q=>nx525, A=>state_1, B=>abovesearchth, C=>
--       nx474);
--    ix528 : AOI221 port map ( Q=>nx527, A=>nx529, B=>nx466, C=>state_1, D=>
--       nx14);
--    ix530 : NOR40 port map ( Q=>nx529, A=>nx186, B=>food_EXMPLR, C=>nx170, D
--       =>nx164);
--    ix187 : NOR21 port map ( Q=>nx186, A=>nx477, B=>nx532);
--    ix533 : AOI211 port map ( Q=>nx532, A=>lostfood, B=>state_0, C=>nx180);
--    ix181 : AOI211 port map ( Q=>nx180, A=>nx505, B=>scantimeup, C=>
--       abovesearchth);
--    ix171 : NOR31 port map ( Q=>nx170, A=>nx507, B=>nx494, C=>state_0);
--    ix165 : AOI2111 port map ( Q=>nx164, A=>state_0, B=>athome, C=>nx474, D=>
--       nx482);
--    ix15 : OAI211 port map ( Q=>nx14, A=>athome, B=>nx505, C=>nx538);
--    ix541 : AOI311 port map ( Q=>nx540, A=>nx24, B=>findfood, C=>nx505, D=>
--       nx164);
--    ix543 : CLKIN1 port map ( Q=>nx542, A=>closetofood);
--    ix31 : NOR40 port map ( Q=>search, A=>state_1, B=>state_2, C=>nx545, D=>
--       nx505);
--    ix546 : CLKIN1 port map ( Q=>nx545, A=>aboverestth);
--    ix539 : MUX22 port map ( Q=>nx538, A=>state_0, B=>nx474, S=>nx465);
-- end Classic ;
--
-- architecture Gray of Robot is
--    signal rest_EXMPLR, food_EXMPLR, state_1, state_0, nx488, nx10, state_3,
--       nx26, state_2, nx40, nx50, nx490, nx56, nx76, nx82, nx92, nx104, nx118,
--       nx120, nx140, nx148, nx174, nx498, nx501, nx504, nx507, nx510, nx514,
--       nx516, nx519, nx522, nx526, nx529, nx532, nx534, nx537, nx540, nx544,
--       nx546, nx550, nx553, nx555, nx557, nx559, nx562, nx565, nx567, nx570:
--    std_logic ;
--
-- -- PSL default clock is (clk'event and clk = '1');
--
-- -- PSL property P1 is
-- -- always (
-- --   (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- --   -> (not (state_3 = '0' and state_2 = '1' and state_1 = '0' and state_0 = '1')) until! findfood = '1'
-- -- );
-- -- PSL assert P1;
--
-- -- PSL property P2 is
-- -- always (
-- --   (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- --   -> (not (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '0'))
-- --      until! abovesearchth = '1'
-- -- );
-- -- PSL assert P2;
--
-- -- PSL property P3 is
-- -- always (
-- --   (state_3 = '0' and state_2 = '1' and state_1 = '0' and state_0 = '1')
-- --   -> (eventually! (state_3 = '1' and state_2 = '1' and state_1 = '0' and state_0 = '0'))
-- --      and
-- --      (eventually! (state_3 = '0' and state_2 = '0' and state_1 = '0' and state_0 = '1'))
-- -- );
-- -- PSL assert P3;
--
-- -- PSL property P4 is
-- -- always (
-- --   { (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- --   ; (abovesearchth = '0' and findfood = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '1')
-- --   ; (abovesearchth = '0' and lostfood = '0')[*]
-- --   ; (abovesearchth = '0' and lostfood = '1')
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
-- --   }
-- --   |-> next! (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- -- );
-- -- PSL assert P4;
--
-- -- PSL property P5 is
-- -- always (
-- --   (state_3 = '0' and state_2 = '0' and state_1 = '0' and state_0 = '1')
-- --   -> eventually! (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- -- );
-- -- PSL assert P5;
--
-- begin
--    rest <= rest_EXMPLR ;
--    food <= food_EXMPLR ;
--    ix185 : CLKIN1 port map ( Q=>food_EXMPLR, A=>nx498);
--    ix499 : NAND31 port map ( Q=>nx498, A=>nx488, B=>closetofood, C=>state_0
--    );
--    ix181 : NOR21 port map ( Q=>nx488, A=>nx501, B=>nx544);
--    ix175 : OAI2111 port map ( Q=>nx174, A=>food_EXMPLR, B=>nx504, C=>nx565,
--       D=>nx567);
--    ix505 : OAI2111 port map ( Q=>nx504, A=>nx26, B=>state_2, C=>nx519, D=>
--       state_1);
--    ix27 : NAND21 port map ( Q=>nx26, A=>nx507, B=>athome);
--    ix149 : OAI2111 port map ( Q=>nx148, A=>lostfood, B=>nx510, C=>nx516, D=>
--       nx559);
--    ix511 : AOI211 port map ( Q=>nx510, A=>nx488, B=>nx10, C=>food_EXMPLR);
--    ix11 : OAI211 port map ( Q=>nx10, A=>abovesearchth, B=>nx507, C=>state_1
--    );
--    reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx501, C=>clk, D=>nx174, RN
--       =>nx514);
--    ix515 : CLKIN1 port map ( Q=>nx514, A=>reset);
--    ix517 : AOI221 port map ( Q=>nx516, A=>success, B=>nx490, C=>nx40, D=>
--       nx140);
--    ix99 : NOR21 port map ( Q=>nx490, A=>nx519, B=>nx534);
--    ix105 : OAI221 port map ( Q=>nx104, A=>nx26, B=>nx522, C=>success, D=>
--       nx526);
--    ix523 : NAND31 port map ( Q=>nx522, A=>nx501, B=>nx519, C=>state_2);
--    ix93 : OAI2111 port map ( Q=>nx92, A=>success, B=>nx526, C=>nx498, D=>
--       nx529);
--    ix527 : NAND21 port map ( Q=>nx526, A=>state_3, B=>state_2);
--    reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx519, C=>clk, D=>nx104, RN
--       =>nx514);
--    ix530 : AOI311 port map ( Q=>nx529, A=>nx50, B=>findfood, C=>nx537, D=>
--       nx82);
--    ix51 : OAI221 port map ( Q=>nx50, A=>nx501, B=>nx532, C=>state_3, D=>
--       state_0);
--    ix533 : NAND21 port map ( Q=>nx532, A=>nx519, B=>nx534);
--    reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx534, C=>clk, D=>nx92, RN
--       =>nx514);
--    reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx507, C=>clk, D=>nx148, RN
--       =>nx514);
--    ix538 : OAI211 port map ( Q=>nx537, A=>nx56, B=>nx40, C=>nx507);
--    ix57 : NOR40 port map ( Q=>nx56, A=>nx519, B=>nx534, C=>nx540, D=>state_1
--    );
--    ix541 : CLKIN1 port map ( Q=>nx540, A=>success);
--    ix41 : NOR21 port map ( Q=>nx40, A=>state_3, B=>state_2);
--    ix83 : NOR21 port map ( Q=>nx82, A=>nx544, B=>nx546);
--    ix545 : NAND21 port map ( Q=>nx544, A=>nx519, B=>state_2);
--    ix547 : AOI2111 port map ( Q=>nx546, A=>lostfood, B=>state_0, C=>nx76, D
--       =>nx10);
--    ix77 : NOR21 port map ( Q=>nx76, A=>abovesearchth, B=>scantimeup);
--    ix141 : OAI2111 port map ( Q=>nx140, A=>nx550, B=>rest_EXMPLR, C=>nx555,
--       D=>nx26);
--    ix551 : CLKIN1 port map ( Q=>nx550, A=>findfood);
--    ix63 : NOR21 port map ( Q=>rest_EXMPLR, A=>state_0, B=>nx553);
--    ix554 : AOI311 port map ( Q=>nx553, A=>nx490, B=>success, C=>nx501, D=>
--       nx40);
--    ix556 : AOI211 port map ( Q=>nx555, A=>nx557, B=>state_0, C=>nx501);
--    ix558 : CLKIN1 port map ( Q=>nx557, A=>abovesearchth);
--    ix560 : AOI311 port map ( Q=>nx559, A=>nx120, B=>nx507, C=>nx488, D=>
--       nx118);
--    ix121 : NAND21 port map ( Q=>nx120, A=>nx562, B=>nx550);
--    ix563 : CLKIN1 port map ( Q=>nx562, A=>scantimeup);
--    ix119 : NOR40 port map ( Q=>nx118, A=>success, B=>nx507, C=>nx544, D=>
--       state_1);
--    ix566 : NAND21 port map ( Q=>nx565, A=>lostfood, B=>food_EXMPLR);
--    ix568 : NAND41 port map ( Q=>nx567, A=>aboverestth, B=>nx501, C=>state_0,
--       D=>nx40);
--    ix157 : NOR40 port map ( Q=>search, A=>nx570, B=>nx507, C=>state_3, D=>
--       state_2);
--    ix571 : NAND21 port map ( Q=>nx570, A=>aboverestth, B=>nx501);
-- end Gray ;
--
-- architecture OneHot of Robot is
--    signal nx720, state_1, state_0, state_8, state_5, state_4, nx28, nx32,
--       state_2, nx50, nx60, nx76, nx88, nx100, state_3, nx120, nx126, nx140,
--       nx150, nx739, nx741, nx744, nx748, nx750, nx752, nx755, nx758, nx762,
--       nx764, nx766, nx770, nx773, nx775, nx778, nx779, nx782, nx784, nx788,
--       nx790, nx793, nx795, nx798, nx800: std_logic ;
--     -- added by us for either PSL assertions
--     signal state_6, state_7 : std_logic ;
--
-- -- PSL default clock is (clk'event and clk = '1');
--
-- -- PSL property P1 is
-- -- always (
-- --   state_2 = '1'
-- --   -> (not (state_6 = '1')) until! findfood = '1'
-- -- );
-- -- PSL assert P1;
--
-- -- PSL property P2 is
-- -- always (
-- --   state_2 = '1'
-- --   -> (not (state_3 = '1')) until! abovesearchth = '1'
-- -- );
-- -- PSL assert P2;
--
-- -- PSL property P3 is
-- -- always (
-- --   state_6 = '1'
-- --   -> (eventually! state_8 = '1') and (eventually! state_1 = '1')
-- -- );
-- -- PSL assert P3;
--
-- -- PSL property P4 is
-- -- always (
-- --   { (state_2 = '1')
-- --   ; (abovesearchth = '0' and findfood = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '1')
-- --   ; (abovesearchth = '0' and lostfood = '0')[*]
-- --   ; (abovesearchth = '0' and lostfood = '1')
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
-- --   }
-- --   |-> next! (state_2 = '1')
-- -- );
-- -- PSL assert P4;
--
-- -- PSL property P5 is
-- -- always (
-- --   state_1 = '1' -> eventually! (state_2 = '1')
-- -- );
-- -- PSL assert P5;
--
-- begin
--    ix721 : TIE0 port map ( Q=>nx720);
--    ix157 : NOR21 port map ( Q=>search, A=>nx739, B=>nx741);
--    ix740 : CLKIN1 port map ( Q=>nx739, A=>aboverestth);
--    ix151 : OAI2111 port map ( Q=>nx150, A=>aboverestth, B=>nx741, C=>nx744,
--       D=>nx784);
--    ix745 : NAND21 port map ( Q=>nx744, A=>athome, B=>state_3);
--    ix141 : OAI221 port map ( Q=>nx140, A=>nx748, B=>nx750, C=>athome, D=>
--       nx782);
--    ix749 : CLKIN1 port map ( Q=>nx748, A=>abovesearchth);
--    ix751 : AOI211 port map ( Q=>nx750, A=>nx752, B=>nx126, C=>nx120);
--    ix753 : CLKIN1 port map ( Q=>nx752, A=>findfood);
--    ix127 : OAI211 port map ( Q=>nx126, A=>scantimeup, B=>nx755, C=>nx778);
--    ix33 : CLKIN1 port map ( Q=>nx32, A=>nx758);
--    ix759 : AOI211 port map ( Q=>nx758, A=>lostfood, B=>state_5, C=>nx28);
--    reg_state_5 : DFC1 port map ( Q=>state_5, QN=>nx779, C=>clk, D=>nx60, RN
--       =>nx773);
--    ix61 : OAI311 port map ( Q=>nx60, A=>lostfood, B=>abovesearchth, C=>nx762,
--       D=>nx766);
--    ix763 : NAND21 port map ( Q=>nx762, A=>nx764, B=>state_5);
--    ix765 : CLKIN1 port map ( Q=>nx764, A=>closetofood);
--    ix767 : OAI211 port map ( Q=>nx766, A=>state_2, B=>state_4, C=>findfood);
--    reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx778, C=>clk, D=>nx50, RN
--       =>nx773);
--    ix51 : OAI211 port map ( Q=>nx50, A=>findfood, B=>nx770, C=>nx775);
--    ix771 : AOI221 port map ( Q=>nx770, A=>scantimeup, B=>state_4, C=>nx748,
--       D=>state_2);
--    reg_state_4 : DFC1 port map ( Q=>state_4, QN=>nx755, C=>clk, D=>nx32, RN
--       =>nx773);
--    ix774 : CLKIN1 port map ( Q=>nx773, A=>reset);
--    ix776 : NAND21 port map ( Q=>nx775, A=>aboverestth, B=>state_1);
--    reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx741, C=>clk, D=>nx150, RN
--       =>nx773);
--    ix29 : NOR40 port map ( Q=>nx28, A=>scantimeup, B=>abovesearchth, C=>
--       findfood, D=>nx755);
--    ix121 : NOR31 port map ( Q=>nx120, A=>nx779, B=>lostfood, C=>closetofood
--    );
--    reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx782, C=>clk, D=>nx140, RN
--       =>nx773);
--    ix785 : AOI211 port map ( Q=>nx784, A=>success, B=>state_8, C=>state_0);
--    ix101 : OAI221 port map ( Q=>nx100, A=>nx788, B=>nx790, C=>success, D=>
--       nx800);
--    ix789 : CLKIN1 port map ( Q=>nx788, A=>athome);
-- -- reg_state_7 : DFC1 port map ( Q=>OPEN, QN=>nx790, C=>clk, D=>nx88, RN=>
-- --    nx773);
--    reg_state_7 : DFC1 port map ( Q=>state_7, QN=>nx790, C=>clk, D=>nx88, RN=>
--       nx773);
--    ix89 : OAI221 port map ( Q=>nx88, A=>nx793, B=>nx795, C=>athome, D=>nx790
--    );
--    ix794 : CLKIN1 port map ( Q=>nx793, A=>success);
-- --   reg_state_6 : DFC1 port map ( Q=>OPEN, QN=>nx795, C=>clk, D=>nx76, RN=>
-- --      nx773);
--    reg_state_6 : DFC1 port map ( Q=>state_6, QN=>nx795, C=>clk, D=>nx76, RN=>
--       nx773);
--    ix77 : OAI221 port map ( Q=>nx76, A=>lostfood, B=>nx798, C=>success, D=>
--       nx795);
--    ix799 : NAND21 port map ( Q=>nx798, A=>closetofood, B=>state_5);
--    reg_state_8 : DFC1 port map ( Q=>state_8, QN=>nx800, C=>clk, D=>nx100, RN
--       =>nx773);
--    reg_state_0 : DFP1 port map ( Q=>state_0, QN=>OPEN, C=>clk, D=>nx720, SN
--       =>nx773);
--    ix67 : NOR21 port map ( Q=>food, A=>nx764, B=>nx779);
--    ix159 : NAND21 port map ( Q=>rest, A=>nx784, B=>nx782);
-- end OneHot ;
