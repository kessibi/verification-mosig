#!/bin/bash

# source ../confmodelsim/config 

## nettoyage
if [ -d ../libs/LIB_SYSTEM ] 
then /bin/rm -rf ../libs/LIB_SYSTEM
fi

## creation de la librairie de travail
vlib ../libs/LIB_SYSTEM

## compilation
vcom -work LIB_SYSTEM system.vhd
