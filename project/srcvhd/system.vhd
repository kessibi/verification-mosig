library IEEE;
library LIB_ROBOT;
library LIB_COUNTER;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library c35_corelib;
use c35_corelib.vcomponents.all;

entity System is
    port(reset, clk, athome, findfood, lostfood, closetofood, success, scantimeup: in std_logic;
    food: out std_logic);
end System;

architecture Orig of System is
  component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood,
    success, aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
  end component;

  component Counter is
      generic (threshold : natural);
      port(reset, clk, start : in std_logic;
      aboveth: out std_logic);
  end component;

  signal s_aboverestth, s_abovesearchth, s_rest, s_search: std_logic;

begin
    R : entity LIB_ROBOT.Robot(Orig)
        port map(clk => clk, athome => athome, findfood => findfood,
                       closetofood => closetofood, success => success,
                       scantimeup => scantimeup, aboverestth => s_aboverestth,
                       abovesearchth => s_abovesearchth, reset => reset,
                       lostfood => lostfood, rest => s_rest,
                       search => s_search, food => food);

    C1 : entity LIB_COUNTER.Counter(Orig)
      generic map (threshold => 4)
      port map (clk => clk, reset => reset, start => s_rest, aboveth => s_aboverestth);

    -- searchrest counter
    C2 : entity LIB_COUNTER.Counter(Orig)
      generic map (threshold => 10)
      port map (clk => clk, reset => reset, start => s_search, aboveth => s_abovesearchth);
end Orig;

library IEEE;
use IEEE.std_logic_1164.all;
library c35_corelib;
use c35_corelib.vcomponents.all;

entity LIB_ROBOT_Robot_classic is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end LIB_ROBOT_Robot_classic ;

architecture Classic of LIB_ROBOT_Robot_classic is
   signal rest_EXMPLR, food_EXMPLR, state_2, state_0, state_1, nx465, nx14, 
      nx466, nx24, nx38, nx60, nx74, state_3, nx100, nx116, nx138, nx148, 
      nx164, nx170, nx180, nx186, nx474, nx477, nx480, nx482, nx485, nx491, 
      nx494, nx496, nx498, nx503, nx505, nx507, nx509, nx512, nx514, nx516, 
      nx519, nx522, nx525, nx527, nx529, nx532, nx538, nx540, nx542, nx545: 
   std_logic ;

-- PSL default clock is (clk'event and clk = '1');
--
-- PSL property P1 is
-- always (
--   (state_2 = '0' and state_1 = '1' and state_0 = '0')
--   -> (not (state_2 = '1' and state_1 = '1' and state_0 = '0'))
--      until! findfood = '1'
-- );
-- PSL assert P1;
--
-- PSL property P2 is
-- always (
--   (state_2 = '0' and state_1 = '1' and state_0 = '0')
--   -> (not (state_2 = '0' and state_1 = '1' and state_0 = '1'))
--      until! abovesearchth = '1'
-- );
-- PSL assert P2;
--
-- PSL property P3 is
-- always (
--   (state_2 = '1' and state_1 = '1' and state_0 = '0')
--   -> (eventually! (state_3 = '1' and state_2 = '0' and state_1 = '0' and state_0 = '0'))
--      and
--      (eventually! (state_2 = '0' and state_1 = '0' and state_0 = '1'))
-- );
-- PSL assert P3;
--
-- PSL property P4 is
-- always (
--   { (state_2 = '0' and state_1 = '1' and state_0 = '0')
--   ; (abovesearchth = '0' and findfood = '0')[*]
--   ; (abovesearchth = '0' and findfood = '1')
--   ; (abovesearchth = '0' and lostfood = '0')[*]
--   ; (abovesearchth = '0' and lostfood = '1')
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
--   }
--   |-> next! (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- );
-- PSL assert P4;
--
-- PSL property P5 is
-- always (
--   (state_2 = '0' and state_1 = '0' and state_0 = '1')
--   -> eventually! (state_2 = '0' and state_1 = '1' and state_0 = '0')
-- );
-- PSL assert P5;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix203 : NOR40 port map ( Q=>food_EXMPLR, A=>nx474, B=>state_1, C=>nx542, 
      D=>nx505);
   ix191 : OAI2111 port map ( Q=>nx465, A=>nx477, B=>nx532, C=>nx485, D=>
      nx540);
   ix478 : NAND21 port map ( Q=>nx477, A=>state_2, B=>nx482);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx474, C=>clk, D=>nx465, RN
      =>nx480);
   ix481 : CLKIN1 port map ( Q=>nx480, A=>reset);
   ix39 : OAI2111 port map ( Q=>nx38, A=>lostfood, B=>nx485, C=>nx522, D=>
      nx527);
   ix486 : NAND31 port map ( Q=>nx485, A=>nx466, B=>closetofood, C=>state_0
   );
   ix199 : NOR21 port map ( Q=>nx466, A=>nx474, B=>state_1);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx482, C=>clk, D=>nx38, RN
      =>nx480);
   ix149 : NAND41 port map ( Q=>nx148, A=>nx491, B=>nx498, C=>nx519, D=>
      nx525);
   ix492 : OAI211 port map ( Q=>nx491, A=>nx138, B=>nx474, C=>nx465);
   ix139 : OAI311 port map ( Q=>nx138, A=>state_0, B=>nx494, C=>state_1, D=>
      nx496);
   ix495 : CLKIN1 port map ( Q=>nx494, A=>findfood);
   ix497 : OAI211 port map ( Q=>nx496, A=>state_0, B=>success, C=>state_1);
   ix499 : AOI311 port map ( Q=>nx498, A=>state_0, B=>nx474, C=>state_1, D=>
      nx116);
   ix117 : AOI2111 port map ( Q=>nx116, A=>state_3, B=>nx516, C=>state_0, D
      =>nx24);
   ix101 : AOI211 port map ( Q=>nx100, A=>nx503, B=>nx509, C=>rest_EXMPLR);
   ix504 : NAND21 port map ( Q=>nx503, A=>nx505, B=>nx507);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx505, C=>clk, D=>nx148, RN
      =>nx480);
   ix508 : NOR21 port map ( Q=>nx507, A=>state_1, B=>state_2);
   ix510 : NAND31 port map ( Q=>nx509, A=>state_0, B=>athome, C=>state_1);
   ix125 : OAI311 port map ( Q=>rest_EXMPLR, A=>nx505, B=>state_2, C=>nx482, 
      D=>nx512);
   ix513 : OAI2111 port map ( Q=>nx512, A=>nx514, B=>success, C=>nx505, D=>
      nx507);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx514, C=>clk, D=>nx100, RN
      =>nx480);
   ix517 : CLKIN1 port map ( Q=>nx516, A=>success);
   ix25 : NAND21 port map ( Q=>nx24, A=>nx482, B=>nx474);
   ix520 : AOI311 port map ( Q=>nx519, A=>nx74, B=>state_0, C=>nx522, D=>
      nx60);
   ix75 : OAI311 port map ( Q=>nx74, A=>food_EXMPLR, B=>lostfood, C=>state_1, 
      D=>state_2);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx507, B=>aboverestth, C=>state_0
   );
   ix61 : NOR40 port map ( Q=>nx60, A=>scantimeup, B=>nx474, C=>state_0, D=>
      nx465);
   ix526 : NAND31 port map ( Q=>nx525, A=>state_1, B=>abovesearchth, C=>
      nx474);
   ix528 : AOI221 port map ( Q=>nx527, A=>nx529, B=>nx466, C=>state_1, D=>
      nx14);
   ix530 : NOR40 port map ( Q=>nx529, A=>nx186, B=>food_EXMPLR, C=>nx170, D
      =>nx164);
   ix187 : NOR21 port map ( Q=>nx186, A=>nx477, B=>nx532);
   ix533 : AOI211 port map ( Q=>nx532, A=>lostfood, B=>state_0, C=>nx180);
   ix181 : AOI211 port map ( Q=>nx180, A=>nx505, B=>scantimeup, C=>
      abovesearchth);
   ix171 : NOR31 port map ( Q=>nx170, A=>nx507, B=>nx494, C=>state_0);
   ix165 : AOI2111 port map ( Q=>nx164, A=>state_0, B=>athome, C=>nx474, D=>
      nx482);
   ix15 : OAI211 port map ( Q=>nx14, A=>athome, B=>nx505, C=>nx538);
   ix541 : AOI311 port map ( Q=>nx540, A=>nx24, B=>findfood, C=>nx505, D=>
      nx164);
   ix543 : CLKIN1 port map ( Q=>nx542, A=>closetofood);
   ix31 : NOR40 port map ( Q=>search, A=>state_1, B=>state_2, C=>nx545, D=>
      nx505);
   ix546 : CLKIN1 port map ( Q=>nx545, A=>aboverestth);
   ix539 : MUX22 port map ( Q=>nx538, A=>state_0, B=>nx474, S=>nx465);
end Classic ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

architecture Classic of System is
   component LIB_ROBOT_Robot_classic
      port (
         reset : IN std_logic ;
         clk : IN std_logic ;
         athome : IN std_logic ;
         findfood : IN std_logic ;
         lostfood : IN std_logic ;
         closetofood : IN std_logic ;
         success : IN std_logic ;
         aboverestth : IN std_logic ;
         abovesearchth : IN std_logic ;
         scantimeup : IN std_logic ;
         rest : OUT std_logic ;
         search : OUT std_logic ;
         food : OUT std_logic) ;
   end component ;
   signal s_aboverestth, s_abovesearchth, s_rest, s_search, C2_count_3, 
      C2_state, nx8, nx18, C2_count_2, C2_count_1, C2_count_0, nx36, nx52, 
      nx54, nx70, nx82, C1_count_3, C1_state, nx100, nx110, C1_count_2, 
      C1_count_1, C1_count_0, nx128, nx144, nx56, nx162, nx174, nx67, nx74, 
      nx76, nx79, nx81, nx86, nx89, nx91, nx95, nx97, nx101, nx105, nx107, 
      nx113, nx115, nx117, nx119, nx125, nx129, nx133, nx139, nx141, nx143, 
      nx148, nx150, nx153, nx155, nx161, nx163, nx167, nx169, nx171, nx173: 
   std_logic ;

begin
   R : LIB_ROBOT_Robot_classic port map ( reset=>reset, clk=>clk, athome=>athome, 
      findfood=>findfood, lostfood=>lostfood, closetofood=>closetofood, 
      success=>success, aboverestth=>s_aboverestth, abovesearchth=>
      s_abovesearchth, scantimeup=>scantimeup, rest=>s_rest, search=>
      s_search, food=>food);
   ix91 : AOI211 port map ( Q=>s_abovesearchth, A=>nx67, B=>nx97, C=>nx115);
   ix53 : OAI311 port map ( Q=>nx52, A=>nx67, B=>C2_state, C=>s_search, D=>
      nx81);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx79, C=>clk, D=>nx8, RN
      =>nx76);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>s_search);
   ix78 : CLKIN1 port map ( Q=>nx76, A=>reset);
   ix82 : OAI2111 port map ( Q=>nx81, A=>C2_count_0, B=>C2_count_1, C=>nx95, 
      D=>nx8);
   ix37 : OAI221 port map ( Q=>nx36, A=>C2_state, B=>nx86, C=>
      s_abovesearchth, D=>nx89);
   ix90 : NAND21 port map ( Q=>nx89, A=>nx91, B=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx91, C=>clk, D=>nx36, 
      RN=>nx76);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx67, C=>clk, D=>nx52, 
      RN=>nx76);
   ix96 : NAND21 port map ( Q=>nx95, A=>C2_count_1, B=>C2_count_0);
   ix71 : OAI311 port map ( Q=>nx70, A=>nx101, B=>nx54, C=>nx105, D=>nx119);
   ix102 : AOI211 port map ( Q=>nx101, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx97, C=>clk, D=>nx70, 
      RN=>nx76);
   ix77 : NOR21 port map ( Q=>nx54, A=>nx97, B=>nx95);
   ix108 : OAI211 port map ( Q=>nx107, A=>C2_count_1, B=>C2_count_2, C=>
      C2_count_3);
   ix83 : OAI221 port map ( Q=>nx82, A=>nx113, B=>nx105, C=>nx115, D=>nx117
   );
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx115, C=>clk, D=>
      nx82, RN=>nx76);
   ix118 : NAND21 port map ( Q=>nx117, A=>nx79, B=>nx74);
   ix120 : NAND21 port map ( Q=>nx119, A=>C2_count_2, B=>nx18);
   ix19 : NOR21 port map ( Q=>nx18, A=>C2_state, B=>s_search);
   ix181 : NAND21 port map ( Q=>s_aboverestth, A=>nx125, B=>nx169);
   ix163 : OAI311 port map ( Q=>nx162, A=>nx129, B=>nx56, C=>nx161, D=>nx173
   );
   ix130 : AOI211 port map ( Q=>nx129, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix145 : OAI311 port map ( Q=>nx144, A=>nx133, B=>C1_state, C=>s_rest, D=>
      nx143);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx133, C=>clk, D=>
      nx144, RN=>nx76);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx141, C=>clk, D=>nx100, 
      RN=>nx76);
   ix140 : CLKIN1 port map ( Q=>nx139, A=>s_rest);
   ix144 : OAI2111 port map ( Q=>nx143, A=>C1_count_0, B=>C1_count_1, C=>
      nx155, D=>nx100);
   ix129 : OAI221 port map ( Q=>nx128, A=>C1_state, B=>nx148, C=>
      s_aboverestth, D=>nx150);
   ix152 : NAND21 port map ( Q=>nx150, A=>nx153, B=>C1_state);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx153, C=>clk, D=>
      nx128, RN=>nx76);
   ix156 : NAND21 port map ( Q=>nx155, A=>C1_count_1, B=>C1_count_0);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx125, C=>clk, D=>
      nx162, RN=>nx76);
   ix169 : NOR21 port map ( Q=>nx56, A=>nx125, B=>nx155);
   ix164 : NOR21 port map ( Q=>nx163, A=>C1_count_2, B=>C1_count_3);
   ix175 : OAI221 port map ( Q=>nx174, A=>nx167, B=>nx161, C=>nx169, D=>
      nx171);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx169, C=>clk, D=>
      nx174, RN=>nx76);
   ix172 : NAND21 port map ( Q=>nx171, A=>nx141, B=>nx139);
   ix174 : NAND21 port map ( Q=>nx173, A=>C1_count_2, B=>nx110);
   ix111 : NOR21 port map ( Q=>nx110, A=>C1_state, B=>s_rest);
   ix9 : IMUX21 port map ( Q=>nx8, A=>s_abovesearchth, B=>nx74, S=>nx79);
   ix87 : XOR21 port map ( Q=>nx86, A=>nx91, B=>s_search);
   ix106 : IMUX21 port map ( Q=>nx105, A=>nx107, B=>s_search, S=>nx79);
   ix114 : XOR21 port map ( Q=>nx113, A=>nx115, B=>nx54);
   ix101 : IMUX21 port map ( Q=>nx100, A=>s_aboverestth, B=>nx139, S=>nx141
   );
   ix149 : XOR21 port map ( Q=>nx148, A=>nx153, B=>s_rest);
   ix162 : IMUX21 port map ( Q=>nx161, A=>nx163, B=>s_rest, S=>nx141);
   ix168 : XOR21 port map ( Q=>nx167, A=>nx169, B=>nx56);
end Classic ;

library IEEE;
use IEEE.std_logic_1164.all;
library c35_corelib;
use c35_corelib.vcomponents.all;


entity LIB_ROBOT_Robot_gray is
   port (
      reset : IN std_logic ;
      clk : IN std_logic ;
      athome : IN std_logic ;
      findfood : IN std_logic ;
      lostfood : IN std_logic ;
      closetofood : IN std_logic ;
      success : IN std_logic ;
      aboverestth : IN std_logic ;
      abovesearchth : IN std_logic ;
      scantimeup : IN std_logic ;
      rest : OUT std_logic ;
      search : OUT std_logic ;
      food : OUT std_logic) ;
end LIB_ROBOT_Robot_gray ;

architecture Gray of LIB_ROBOT_Robot_gray is
   signal rest_EXMPLR, food_EXMPLR, state_1, state_0, nx488, nx10, state_3, 
      nx26, state_2, nx40, nx50, nx490, nx56, nx76, nx82, nx92, nx104, nx118, 
      nx120, nx140, nx148, nx174, nx498, nx501, nx504, nx507, nx510, nx514, 
      nx516, nx519, nx522, nx526, nx529, nx532, nx534, nx537, nx540, nx544, 
      nx546, nx550, nx553, nx555, nx557, nx559, nx562, nx565, nx567, nx570: 
   std_logic ;

-- PSL default clock is (clk'event and clk = '1');
--
-- PSL property P1 is
-- always (
--   (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
--   -> (not (state_3 = '0' and state_2 = '1' and state_1 = '0' and state_0 = '1')) until! findfood = '1'
-- );
-- PSL assert P1;
--
-- PSL property P2 is
-- always (
--   (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
--   -> (not (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '0'))
--      until! abovesearchth = '1'
-- );
-- PSL assert P2;
--
-- PSL property P3 is
-- always (
--   (state_3 = '0' and state_2 = '1' and state_1 = '0' and state_0 = '1')
--   -> (eventually! (state_3 = '1' and state_2 = '1' and state_1 = '0' and state_0 = '0'))
--      and
--      (eventually! (state_3 = '0' and state_2 = '0' and state_1 = '0' and state_0 = '1'))
-- );
-- PSL assert P3;
--
-- PSL property P4 is
-- always (
--   { (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
--   ; (abovesearchth = '0' and findfood = '0')[*]
--   ; (abovesearchth = '0' and findfood = '1')
--   ; (abovesearchth = '0' and lostfood = '0')[*]
--   ; (abovesearchth = '0' and lostfood = '1')
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
--   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
--   }
--   |-> next! (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- );
-- PSL assert P4;
--
-- PSL property P5 is
-- always (
--   (state_3 = '0' and state_2 = '0' and state_1 = '0' and state_0 = '1')
--   -> eventually! (state_3 = '0' and state_2 = '0' and state_1 = '1' and state_0 = '1')
-- );
-- PSL assert P5;

begin
   rest <= rest_EXMPLR ;
   food <= food_EXMPLR ;
   ix185 : CLKIN1 port map ( Q=>food_EXMPLR, A=>nx498);
   ix499 : NAND31 port map ( Q=>nx498, A=>nx488, B=>closetofood, C=>state_0
   );
   ix181 : NOR21 port map ( Q=>nx488, A=>nx501, B=>nx544);
   ix175 : OAI2111 port map ( Q=>nx174, A=>food_EXMPLR, B=>nx504, C=>nx565, 
      D=>nx567);
   ix505 : OAI2111 port map ( Q=>nx504, A=>nx26, B=>state_2, C=>nx519, D=>
      state_1);
   ix27 : NAND21 port map ( Q=>nx26, A=>nx507, B=>athome);
   ix149 : OAI2111 port map ( Q=>nx148, A=>lostfood, B=>nx510, C=>nx516, D=>
      nx559);
   ix511 : AOI211 port map ( Q=>nx510, A=>nx488, B=>nx10, C=>food_EXMPLR);
   ix11 : OAI211 port map ( Q=>nx10, A=>abovesearchth, B=>nx507, C=>state_1
   );
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx501, C=>clk, D=>nx174, RN
      =>nx514);
   ix515 : CLKIN1 port map ( Q=>nx514, A=>reset);
   ix517 : AOI221 port map ( Q=>nx516, A=>success, B=>nx490, C=>nx40, D=>
      nx140);
   ix99 : NOR21 port map ( Q=>nx490, A=>nx519, B=>nx534);
   ix105 : OAI221 port map ( Q=>nx104, A=>nx26, B=>nx522, C=>success, D=>
      nx526);
   ix523 : NAND31 port map ( Q=>nx522, A=>nx501, B=>nx519, C=>state_2);
   ix93 : OAI2111 port map ( Q=>nx92, A=>success, B=>nx526, C=>nx498, D=>
      nx529);
   ix527 : NAND21 port map ( Q=>nx526, A=>state_3, B=>state_2);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx519, C=>clk, D=>nx104, RN
      =>nx514);
   ix530 : AOI311 port map ( Q=>nx529, A=>nx50, B=>findfood, C=>nx537, D=>
      nx82);
   ix51 : OAI221 port map ( Q=>nx50, A=>nx501, B=>nx532, C=>state_3, D=>
      state_0);
   ix533 : NAND21 port map ( Q=>nx532, A=>nx519, B=>nx534);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx534, C=>clk, D=>nx92, RN
      =>nx514);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx507, C=>clk, D=>nx148, RN
      =>nx514);
   ix538 : OAI211 port map ( Q=>nx537, A=>nx56, B=>nx40, C=>nx507);
   ix57 : NOR40 port map ( Q=>nx56, A=>nx519, B=>nx534, C=>nx540, D=>state_1
   );
   ix541 : CLKIN1 port map ( Q=>nx540, A=>success);
   ix41 : NOR21 port map ( Q=>nx40, A=>state_3, B=>state_2);
   ix83 : NOR21 port map ( Q=>nx82, A=>nx544, B=>nx546);
   ix545 : NAND21 port map ( Q=>nx544, A=>nx519, B=>state_2);
   ix547 : AOI2111 port map ( Q=>nx546, A=>lostfood, B=>state_0, C=>nx76, D
      =>nx10);
   ix77 : NOR21 port map ( Q=>nx76, A=>abovesearchth, B=>scantimeup);
   ix141 : OAI2111 port map ( Q=>nx140, A=>nx550, B=>rest_EXMPLR, C=>nx555, 
      D=>nx26);
   ix551 : CLKIN1 port map ( Q=>nx550, A=>findfood);
   ix63 : NOR21 port map ( Q=>rest_EXMPLR, A=>state_0, B=>nx553);
   ix554 : AOI311 port map ( Q=>nx553, A=>nx490, B=>success, C=>nx501, D=>
      nx40);
   ix556 : AOI211 port map ( Q=>nx555, A=>nx557, B=>state_0, C=>nx501);
   ix558 : CLKIN1 port map ( Q=>nx557, A=>abovesearchth);
   ix560 : AOI311 port map ( Q=>nx559, A=>nx120, B=>nx507, C=>nx488, D=>
      nx118);
   ix121 : NAND21 port map ( Q=>nx120, A=>nx562, B=>nx550);
   ix563 : CLKIN1 port map ( Q=>nx562, A=>scantimeup);
   ix119 : NOR40 port map ( Q=>nx118, A=>success, B=>nx507, C=>nx544, D=>
      state_1);
   ix566 : NAND21 port map ( Q=>nx565, A=>lostfood, B=>food_EXMPLR);
   ix568 : NAND41 port map ( Q=>nx567, A=>aboverestth, B=>nx501, C=>state_0, 
      D=>nx40);
   ix157 : NOR40 port map ( Q=>search, A=>nx570, B=>nx507, C=>state_3, D=>
      state_2);
   ix571 : NAND21 port map ( Q=>nx570, A=>aboverestth, B=>nx501);
end Gray ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

architecture Gray of System is
   component LIB_ROBOT_Robot_gray
      port (
         reset : IN std_logic ;
         clk : IN std_logic ;
         athome : IN std_logic ;
         findfood : IN std_logic ;
         lostfood : IN std_logic ;
         closetofood : IN std_logic ;
         success : IN std_logic ;
         aboverestth : IN std_logic ;
         abovesearchth : IN std_logic ;
         scantimeup : IN std_logic ;
         rest : OUT std_logic ;
         search : OUT std_logic ;
         food : OUT std_logic) ;
   end component ;
   signal s_aboverestth, s_abovesearchth, s_rest, s_search, C2_count_3, 
      C2_state, nx8, nx18, C2_count_2, C2_count_1, C2_count_0, nx36, nx52, 
      nx54, nx70, nx82, C1_count_3, C1_state, nx100, nx110, C1_count_2, 
      C1_count_1, C1_count_0, nx128, nx144, nx56, nx162, nx174, nx67, nx74, 
      nx76, nx79, nx81, nx86, nx89, nx91, nx95, nx97, nx101, nx105, nx107, 
      nx113, nx115, nx117, nx119, nx125, nx129, nx133, nx139, nx141, nx143, 
      nx148, nx150, nx153, nx155, nx161, nx163, nx167, nx169, nx171, nx173: 
   std_logic ;

begin
   R : LIB_ROBOT_Robot_gray port map ( reset=>reset, clk=>clk, athome=>athome, 
      findfood=>findfood, lostfood=>lostfood, closetofood=>closetofood, 
      success=>success, aboverestth=>s_aboverestth, abovesearchth=>
      s_abovesearchth, scantimeup=>scantimeup, rest=>s_rest, search=>
      s_search, food=>food);
   ix91 : AOI211 port map ( Q=>s_abovesearchth, A=>nx67, B=>nx97, C=>nx115);
   ix53 : OAI311 port map ( Q=>nx52, A=>nx67, B=>C2_state, C=>s_search, D=>
      nx81);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx79, C=>clk, D=>nx8, RN
      =>nx76);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>s_search);
   ix78 : CLKIN1 port map ( Q=>nx76, A=>reset);
   ix82 : OAI2111 port map ( Q=>nx81, A=>C2_count_0, B=>C2_count_1, C=>nx95, 
      D=>nx8);
   ix37 : OAI221 port map ( Q=>nx36, A=>C2_state, B=>nx86, C=>
      s_abovesearchth, D=>nx89);
   ix90 : NAND21 port map ( Q=>nx89, A=>nx91, B=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx91, C=>clk, D=>nx36, 
      RN=>nx76);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx67, C=>clk, D=>nx52, 
      RN=>nx76);
   ix96 : NAND21 port map ( Q=>nx95, A=>C2_count_1, B=>C2_count_0);
   ix71 : OAI311 port map ( Q=>nx70, A=>nx101, B=>nx54, C=>nx105, D=>nx119);
   ix102 : AOI211 port map ( Q=>nx101, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx97, C=>clk, D=>nx70, 
      RN=>nx76);
   ix77 : NOR21 port map ( Q=>nx54, A=>nx97, B=>nx95);
   ix108 : OAI211 port map ( Q=>nx107, A=>C2_count_1, B=>C2_count_2, C=>
      C2_count_3);
   ix83 : OAI221 port map ( Q=>nx82, A=>nx113, B=>nx105, C=>nx115, D=>nx117
   );
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx115, C=>clk, D=>
      nx82, RN=>nx76);
   ix118 : NAND21 port map ( Q=>nx117, A=>nx79, B=>nx74);
   ix120 : NAND21 port map ( Q=>nx119, A=>C2_count_2, B=>nx18);
   ix19 : NOR21 port map ( Q=>nx18, A=>C2_state, B=>s_search);
   ix181 : NAND21 port map ( Q=>s_aboverestth, A=>nx125, B=>nx169);
   ix163 : OAI311 port map ( Q=>nx162, A=>nx129, B=>nx56, C=>nx161, D=>nx173
   );
   ix130 : AOI211 port map ( Q=>nx129, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix145 : OAI311 port map ( Q=>nx144, A=>nx133, B=>C1_state, C=>s_rest, D=>
      nx143);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx133, C=>clk, D=>
      nx144, RN=>nx76);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx141, C=>clk, D=>nx100, 
      RN=>nx76);
   ix140 : CLKIN1 port map ( Q=>nx139, A=>s_rest);
   ix144 : OAI2111 port map ( Q=>nx143, A=>C1_count_0, B=>C1_count_1, C=>
      nx155, D=>nx100);
   ix129 : OAI221 port map ( Q=>nx128, A=>C1_state, B=>nx148, C=>
      s_aboverestth, D=>nx150);
   ix152 : NAND21 port map ( Q=>nx150, A=>nx153, B=>C1_state);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx153, C=>clk, D=>
      nx128, RN=>nx76);
   ix156 : NAND21 port map ( Q=>nx155, A=>C1_count_1, B=>C1_count_0);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx125, C=>clk, D=>
      nx162, RN=>nx76);
   ix169 : NOR21 port map ( Q=>nx56, A=>nx125, B=>nx155);
   ix164 : NOR21 port map ( Q=>nx163, A=>C1_count_2, B=>C1_count_3);
   ix175 : OAI221 port map ( Q=>nx174, A=>nx167, B=>nx161, C=>nx169, D=>
      nx171);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx169, C=>clk, D=>
      nx174, RN=>nx76);
   ix172 : NAND21 port map ( Q=>nx171, A=>nx141, B=>nx139);
   ix174 : NAND21 port map ( Q=>nx173, A=>C1_count_2, B=>nx110);
   ix111 : NOR21 port map ( Q=>nx110, A=>C1_state, B=>s_rest);
   ix9 : IMUX21 port map ( Q=>nx8, A=>s_abovesearchth, B=>nx74, S=>nx79);
   ix87 : XOR21 port map ( Q=>nx86, A=>nx91, B=>s_search);
   ix106 : IMUX21 port map ( Q=>nx105, A=>nx107, B=>s_search, S=>nx79);
   ix114 : XOR21 port map ( Q=>nx113, A=>nx115, B=>nx54);
   ix101 : IMUX21 port map ( Q=>nx100, A=>s_aboverestth, B=>nx139, S=>nx141
   );
   ix149 : XOR21 port map ( Q=>nx148, A=>nx153, B=>s_rest);
   ix162 : IMUX21 port map ( Q=>nx161, A=>nx163, B=>s_rest, S=>nx141);
   ix168 : XOR21 port map ( Q=>nx167, A=>nx169, B=>nx56);
end Gray ;

architecture OneHot of System is
   signal nx69, R_state_5, C2_count_3, C2_state, R_state_1, R_state_0, 
      R_state_8, nx18, nx30, nx42, R_state_3, nx79, nx58, R_state_2, nx81, 
      R_state_4, nx70, nx74, nx88, nx98, nx114, nx120, C1_count_3, C1_state, 
      nx83, nx130, nx140, C1_count_2, C1_count_1, C1_count_0, nx156, nx172, 
      nx87, nx190, nx202, nx218, nx234, nx244, C2_count_2, C2_count_1, 
      C2_count_0, nx260, nx276, nx90, nx294, nx306, nx332, nx99, nx103, 
      nx105, nx107, nx113, nx118, nx121, nx123, nx127, nx129, nx133, nx135, 
      nx138, nx143, nx147, nx149, nx151, nx155, nx160, nx169, nx175, nx177, 
      nx179, nx183, nx189, nx193, nx195, nx199, nx205, nx207, nx211, nx217, 
      nx219, nx224, nx229, nx231, nx237, nx239, nx242, nx245, nx249, nx253, 
      nx255, nx259, nx261, nx263, nx265, nx269, nx275, nx277, nx279, nx282: 
   std_logic ;
-- 
-- -- PSL default clock is (clk'event and clk = '1');
-- --
-- -- PSL property P1 is
-- -- always (
-- --   (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '1')
-- --   -> (not (R_state_3 = '0' and R_state_2 = '1' and R_state_1 = '0' and R_state_0 = '1')) until! findfood = '1'
-- -- );
-- -- PSL assert P1;
-- --
-- -- PSL property P2 is
-- -- always (
-- --   (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '1')
-- --   -> (not (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '0'))
-- --      until! abovesearchth = '1'
-- -- );
-- -- PSL assert P2;
-- --
-- -- PSL property P3 is
-- -- always (
-- --   (R_state_3 = '0' and R_state_2 = '1' and R_state_1 = '0' and R_state_0 = '1')
-- --   -> (eventually! (R_state_3 = '1' and R_state_2 = '1' and R_state_1 = '0' and R_state_0 = '0'))
-- --      and
-- --      (eventually! (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '0' and R_state_0 = '1'))
-- -- );
-- -- PSL assert P3;
-- --
-- -- PSL property P4 is
-- -- always (
-- --   { (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '1')
-- --   ; (abovesearchth = '0' and findfood = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '1')
-- --   ; (abovesearchth = '0' and lostfood = '0')[*]
-- --   ; (abovesearchth = '0' and lostfood = '1')
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '0')[*]
-- --   ; (abovesearchth = '0' and findfood = '0' and scantimeup = '1')
-- --   }
-- --   |-> next! (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '1')
-- -- );
-- -- PSL assert P4;
-- --
-- -- PSL property P5 is
-- -- always (
-- --   (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '0' and R_state_0 = '1')
-- --   -> eventually! (R_state_3 = '0' and R_state_2 = '0' and R_state_1 = '1' and R_state_0 = '1')
-- -- );
-- -- PSL assert P5;
-- 
begin
   ix70 : TIE0 port map ( Q=>nx69);
   ix339 : CLKIN1 port map ( Q=>food, A=>nx99);
   ix100 : NAND21 port map ( Q=>nx99, A=>closetofood, B=>R_state_5);
   ix333 : OAI311 port map ( Q=>nx332, A=>lostfood, B=>nx103, C=>nx107, D=>
      nx282);
   R_reg_state_5 : DFC1 port map ( Q=>R_state_5, QN=>nx103, C=>clk, D=>nx332, 
      RN=>nx105);
   ix106 : INV3 port map ( Q=>nx105, A=>reset);
   ix108 : NAND21 port map ( Q=>nx107, A=>nx79, B=>nx99);
   ix321 : OAI211 port map ( Q=>nx79, A=>C2_count_2, B=>C2_count_1, C=>
      C2_count_3);
   ix295 : OAI311 port map ( Q=>nx294, A=>nx113, B=>nx90, C=>nx263, D=>nx279
   );
   ix114 : AOI211 port map ( Q=>nx113, A=>C2_count_1, B=>C2_count_0, C=>
      C2_count_2);
   ix277 : OAI211 port map ( Q=>nx276, A=>nx118, B=>nx121, C=>nx277);
   C2_reg_count_1 : DFC1 port map ( Q=>C2_count_1, QN=>nx118, C=>clk, D=>
      nx276, RN=>nx105);
   ix122 : OAI211 port map ( Q=>nx121, A=>nx123, B=>nx83, C=>nx253);
   ix219 : OAI2111 port map ( Q=>nx218, A=>nx127, B=>nx129, C=>nx275, D=>
      nx229);
   ix128 : CLKIN1 port map ( Q=>nx127, A=>athome);
   ix115 : OAI221 port map ( Q=>nx114, A=>nx79, B=>nx133, C=>athome, D=>
      nx129);
   ix134 : AOI211 port map ( Q=>nx133, A=>nx135, B=>nx98, C=>nx58);
   ix136 : CLKIN1 port map ( Q=>nx135, A=>findfood);
   ix99 : OAI211 port map ( Q=>nx98, A=>scantimeup, B=>nx138, C=>nx265);
   R_reg_state_4 : DFC1 port map ( Q=>R_state_4, QN=>nx138, C=>clk, D=>nx74, 
      RN=>nx105);
   ix75 : CLKIN1 port map ( Q=>nx74, A=>nx143);
   ix144 : AOI211 port map ( Q=>nx143, A=>lostfood, B=>R_state_5, C=>nx70);
   ix71 : NOR40 port map ( Q=>nx70, A=>scantimeup, B=>findfood, C=>nx138, D
      =>nx147);
   ix148 : AOI211 port map ( Q=>nx147, A=>nx149, B=>nx118, C=>nx151);
   C2_reg_count_2 : DFC1 port map ( Q=>C2_count_2, QN=>nx149, C=>clk, D=>
      nx294, RN=>nx105);
   ix307 : OAI221 port map ( Q=>nx306, A=>nx155, B=>nx263, C=>nx151, D=>
      nx121);
   C2_reg_count_3 : DFC1 port map ( Q=>C2_count_3, QN=>nx151, C=>clk, D=>
      nx306, RN=>nx105);
   ix301 : NOR21 port map ( Q=>nx90, A=>nx149, B=>nx160);
   ix161 : NAND21 port map ( Q=>nx160, A=>C2_count_1, B=>C2_count_0);
   ix261 : OAI211 port map ( Q=>nx260, A=>C2_state, B=>nx255, C=>nx259);
   C2_reg_state : DFC1 port map ( Q=>C2_state, QN=>nx253, C=>clk, D=>nx234, 
      RN=>nx105);
   ix170 : OAI211 port map ( Q=>nx169, A=>C1_count_3, B=>C1_count_2, C=>
      R_state_1);
   ix203 : OAI221 port map ( Q=>nx202, A=>nx175, B=>nx177, C=>nx249, D=>
      nx242);
   C1_reg_count_3 : DFC1 port map ( Q=>C1_count_3, QN=>nx175, C=>clk, D=>
      nx202, RN=>nx105);
   ix178 : NAND21 port map ( Q=>nx177, A=>nx179, B=>nx242);
   ix184 : AOI2111 port map ( Q=>nx183, A=>success, B=>R_state_8, C=>
      R_state_0, D=>R_state_3);
   ix43 : OAI221 port map ( Q=>nx42, A=>nx127, B=>nx189, C=>success, D=>
      nx199);
   R_reg_state_7 : DFC1 port map ( Q=>OPEN, QN=>nx189, C=>clk, D=>nx30, RN=>
      nx105);
   ix31 : OAI221 port map ( Q=>nx30, A=>nx193, B=>nx195, C=>athome, D=>nx189
   );
   ix194 : CLKIN1 port map ( Q=>nx193, A=>success);
   R_reg_state_6 : DFC1 port map ( Q=>OPEN, QN=>nx195, C=>clk, D=>nx18, RN=>
      nx105);
   ix19 : OAI221 port map ( Q=>nx18, A=>lostfood, B=>nx99, C=>success, D=>
      nx195);
   R_reg_state_8 : DFC1 port map ( Q=>R_state_8, QN=>nx199, C=>clk, D=>nx42, 
      RN=>nx105);
   R_reg_state_0 : DFP1 port map ( Q=>R_state_0, QN=>OPEN, C=>clk, D=>nx69, 
      SN=>nx105);
   R_reg_state_3 : DFC1 port map ( Q=>R_state_3, QN=>nx129, C=>clk, D=>nx114, 
      RN=>nx105);
   ix206 : NAND21 port map ( Q=>nx205, A=>nx175, B=>nx207);
   ix191 : OAI311 port map ( Q=>nx190, A=>nx211, B=>nx87, C=>nx242, D=>nx245
   );
   ix212 : AOI211 port map ( Q=>nx211, A=>C1_count_1, B=>C1_count_0, C=>
      C1_count_2);
   ix173 : OAI211 port map ( Q=>nx172, A=>nx217, B=>nx177, C=>nx219);
   C1_reg_count_1 : DFC1 port map ( Q=>C1_count_1, QN=>nx217, C=>clk, D=>
      nx172, RN=>nx105);
   ix220 : OAI2111 port map ( Q=>nx219, A=>C1_count_0, B=>C1_count_1, C=>
      nx239, D=>nx130);
   ix157 : OAI211 port map ( Q=>nx156, A=>C1_state, B=>nx224, C=>nx231);
   C1_reg_state : DFC1 port map ( Q=>C1_state, QN=>nx179, C=>clk, D=>nx130, 
      RN=>nx105);
   ix121 : NAND21 port map ( Q=>nx120, A=>nx229, B=>nx129);
   ix230 : AOI211 port map ( Q=>nx229, A=>success, B=>R_state_8, C=>
      R_state_0);
   ix232 : NAND31 port map ( Q=>nx231, A=>nx83, B=>nx237, C=>C1_state);
   ix213 : NOR21 port map ( Q=>nx83, A=>C1_count_3, B=>C1_count_2);
   C1_reg_count_2 : DFC1 port map ( Q=>C1_count_2, QN=>nx207, C=>clk, D=>
      nx190, RN=>nx105);
   C1_reg_count_0 : DFC1 port map ( Q=>C1_count_0, QN=>nx237, C=>clk, D=>
      nx156, RN=>nx105);
   ix240 : NAND21 port map ( Q=>nx239, A=>C1_count_1, B=>C1_count_0);
   ix197 : NOR21 port map ( Q=>nx87, A=>nx207, B=>nx239);
   ix246 : NAND21 port map ( Q=>nx245, A=>C1_count_2, B=>nx140);
   ix141 : NOR21 port map ( Q=>nx140, A=>C1_state, B=>nx120);
   R_reg_state_1 : DFC1 port map ( Q=>R_state_1, QN=>nx123, C=>clk, D=>nx218, 
      RN=>nx105);
   ix227 : NOR21 port map ( Q=>nx81, A=>nx123, B=>nx83);
   ix260 : NAND31 port map ( Q=>nx259, A=>nx79, B=>nx261, C=>C2_state);
   C2_reg_count_0 : DFC1 port map ( Q=>C2_count_0, QN=>nx261, C=>clk, D=>
      nx260, RN=>nx105);
   ix89 : OAI221 port map ( Q=>nx88, A=>findfood, B=>nx269, C=>nx123, D=>
      nx83);
   ix270 : AOI221 port map ( Q=>nx269, A=>scantimeup, B=>R_state_4, C=>
      R_state_2, D=>nx79);
   R_reg_state_2 : DFC1 port map ( Q=>R_state_2, QN=>nx265, C=>clk, D=>nx88, 
      RN=>nx105);
   ix59 : NOR31 port map ( Q=>nx58, A=>closetofood, B=>nx103, C=>lostfood);
   ix276 : NAND21 port map ( Q=>nx275, A=>R_state_1, B=>nx83);
   ix278 : OAI2111 port map ( Q=>nx277, A=>C2_count_0, B=>C2_count_1, C=>
      nx160, D=>nx234);
   ix280 : NAND21 port map ( Q=>nx279, A=>C2_count_2, B=>nx244);
   ix245 : NOR21 port map ( Q=>nx244, A=>C2_state, B=>nx81);
   ix284 : OAI211 port map ( Q=>nx282, A=>R_state_2, B=>R_state_4, C=>
      findfood);
   ix156 : XOR21 port map ( Q=>nx155, A=>nx151, B=>nx90);
   ix235 : IMUX21 port map ( Q=>nx234, A=>nx147, B=>nx169, S=>nx253);
   ix131 : IMUX21 port map ( Q=>nx130, A=>nx205, B=>nx183, S=>nx179);
   ix226 : XOR21 port map ( Q=>nx224, A=>nx237, B=>nx120);
   ix244 : IMUX21 port map ( Q=>nx242, A=>nx83, B=>nx120, S=>nx179);
   ix250 : XOR21 port map ( Q=>nx249, A=>nx175, B=>nx87);
   ix256 : XOR21 port map ( Q=>nx255, A=>nx261, B=>nx81);
   ix264 : IMUX21 port map ( Q=>nx263, A=>nx79, B=>nx81, S=>nx253);
end OneHot ;

