library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library c35_corelib;
use c35_corelib.vcomponents.all;

entity Counter is generic(threshold : natural := 7);
  port(reset, clk, start : in std_logic;
       aboveth: out std_logic);

end Counter;

architecture Orig of Counter is

  type States is (Idle, Counting);
  signal state, nextstate : States;
  signal nextcount : natural range 0 to 15;
  signal count : natural range 0 to 15;

begin
  -- next state
  process(state, start, count)
  begin
    case state is
      when Idle =>
        if (start = '0') then
          nextstate <= state;
          nextcount <= count;
        else
          nextstate <= Counting;
          nextcount <= count + 1;
        end if;

      when Counting =>
        if (count < threshold) then
          nextstate <= state;
          nextcount <= count + 1;
        else
          nextstate <= Idle;
          nextcount <= 0;
        end if;
    end case;
  end process;

  -- update function
  process (reset, clk)
  begin
    if (reset = '1') then
      state <= Idle;
      count <= 0;
    elsif (clk'event and clk = '1') then
      state <= nextstate;
      count <= nextcount;
    end if;
  end process;

  -- output function
  process(count)
  begin
    if (count >= threshold) then
      aboveth <= '1';
    else
      aboveth <= '0';
    end if;
  end process;
end Orig;

-- architecture Unconstrained of Counter is
--    signal aboveth_EXMPLR, count_30, state, nx8, nx18, count_29, count_26, 
--       count_25, count_22, count_21, count_18, count_17, count_14, count_13, 
--       count_10, count_6, count_5, count_4, count_3, count_2, count_1, 
--       count_0, nx62, nx917, nx76, nx918, nx100, nx919, nx120, nx920, nx138, 
--       nx150, nx162, nx168, count_8, count_7, nx176, nx194, nx208, nx214, 
--       nx220, nx226, count_12, count_11, nx234, nx922, nx252, nx266, nx278, 
--       nx284, count_16, count_15, nx292, nx923, nx310, nx324, nx330, nx336, 
--       nx342, count_20, count_19, nx350, nx358, nx924, nx368, nx382, nx394, 
--       nx400, count_24, count_23, nx408, nx925, nx426, nx440, nx446, nx452, 
--       nx458, count_28, count_27, nx466, nx474, nx926, nx484, nx498, nx504, 
--       nx510, nx528, nx534, nx540, nx554, nx560, nx562, nx566, nx934, nx937, 
--       nx940, nx942, nx945, nx947, nx949, nx952, nx955, nx959, nx961, nx963, 
--       nx967, nx969, nx973, nx975, nx978, nx981, nx985, nx988, nx990, nx994, 
--       nx997, nx999, nx1003, nx1005, nx1007, nx1011, nx1014, nx1018, nx1020, 
--       nx1022, nx1024, nx1027, nx1030, nx1034, nx1037, nx1040, nx1043, nx1047, 
--       nx1051, nx1054, nx1056, nx1058, nx1063, nx1065, nx1069, nx1071, nx1073, 
--       nx1077, nx1080, nx1083, nx1086, nx1090, nx1092, nx1095, nx1098, nx1100, 
--       nx1102, nx1105, nx1109, nx1113, nx1116, nx1120, nx1122, nx1125, nx1127, 
--       nx1131, nx1134, nx1138, nx1140, nx1142, nx1145, nx1149, nx1152, nx1156, 
--       nx1158, nx1160, nx1162, nx1166, nx1169, nx1171, nx1175, nx1178, nx1180, 
--       nx1184, nx1187, nx1191, nx1193, nx1196, nx1198, nx1202, nx1205, nx1209, 
--       nx1212, nx1214, nx1217: std_logic ;
-- 
-- begin
--    aboveth <= aboveth_EXMPLR ;
--    ix571 : NAND41 port map ( Q=>aboveth_EXMPLR, A=>nx934, B=>nx1217, C=>
--       nx1145, D=>nx1180);
--    ix935 : NOR40 port map ( Q=>nx934, A=>nx562, B=>nx918, C=>nx560, D=>nx554
--    );
--    ix563 : NAND21 port map ( Q=>nx562, A=>nx937, B=>nx978);
--    ix121 : OAI211 port map ( Q=>nx120, A=>nx937, B=>nx940, C=>nx949);
--    ix941 : NAND22 port map ( Q=>nx940, A=>nx942, B=>nx945);
--    reg_state : DFC1 port map ( Q=>state, QN=>nx942, C=>clk, D=>nx8, RN=>
--       nx947);
--    ix946 : CLKIN1 port map ( Q=>nx945, A=>start);
--    ix948 : INV3 port map ( Q=>nx947, A=>reset);
--    ix950 : OAI2111 port map ( Q=>nx949, A=>nx918, B=>count_3, C=>nx997, D=>
--       nx8);
--    ix109 : NOR21 port map ( Q=>nx918, A=>nx952, B=>nx917);
--    ix101 : OAI311 port map ( Q=>nx100, A=>nx955, B=>nx918, C=>nx973, D=>
--       nx1214);
--    ix956 : AOI211 port map ( Q=>nx955, A=>count_0, B=>count_1, C=>count_2);
--    ix63 : OAI221 port map ( Q=>nx62, A=>aboveth_EXMPLR, B=>nx959, C=>state, 
--       D=>nx963);
--    ix960 : NAND21 port map ( Q=>nx959, A=>nx961, B=>state);
--    reg_count_0 : DFC1 port map ( Q=>count_0, QN=>nx961, C=>clk, D=>nx62, RN
--       =>nx947);
--    ix77 : OAI211 port map ( Q=>nx76, A=>nx967, B=>nx940, C=>nx969);
--    reg_count_1 : DFC1 port map ( Q=>count_1, QN=>nx967, C=>clk, D=>nx76, RN
--       =>nx947);
--    ix970 : OAI2111 port map ( Q=>nx969, A=>count_0, B=>count_1, C=>nx8, D=>
--       nx917);
--    ix87 : NAND21 port map ( Q=>nx917, A=>count_0, B=>count_1);
--    reg_count_2 : DFC1 port map ( Q=>count_2, QN=>nx952, C=>clk, D=>nx100, RN
--       =>nx947);
--    ix976 : NOR40 port map ( Q=>nx975, A=>nx566, B=>nx554, C=>nx540, D=>nx528
--    );
--    ix567 : NAND41 port map ( Q=>nx566, A=>nx937, B=>nx978, C=>nx988, D=>
--       nx990);
--    ix139 : OAI211 port map ( Q=>nx138, A=>nx978, B=>nx940, C=>nx981);
--    ix982 : OAI2111 port map ( Q=>nx981, A=>nx919, B=>count_4, C=>nx985, D=>
--       nx8);
--    ix127 : NOR40 port map ( Q=>nx919, A=>nx937, B=>nx952, C=>nx961, D=>nx967
--    );
--    reg_count_4 : DFC1 port map ( Q=>count_4, QN=>nx978, C=>clk, D=>nx138, RN
--       =>nx947);
--    ix986 : NAND31 port map ( Q=>nx985, A=>count_4, B=>count_3, C=>nx918);
--    reg_count_3 : DFC1 port map ( Q=>count_3, QN=>nx937, C=>clk, D=>nx120, RN
--       =>nx947);
--    ix989 : NAND31 port map ( Q=>nx988, A=>count_2, B=>count_0, C=>count_1);
--    ix991 : NOR40 port map ( Q=>nx990, A=>count_5, B=>count_6, C=>count_7, D
--       =>count_8);
--    ix151 : OAI221 port map ( Q=>nx150, A=>nx994, B=>nx973, C=>nx999, D=>
--       nx940);
--    ix145 : NOR21 port map ( Q=>nx920, A=>nx978, B=>nx997);
--    ix998 : NAND21 port map ( Q=>nx997, A=>count_3, B=>nx918);
--    reg_count_5 : DFC1 port map ( Q=>count_5, QN=>nx999, C=>clk, D=>nx150, RN
--       =>nx947);
--    ix163 : OAI221 port map ( Q=>nx162, A=>nx1003, B=>nx973, C=>nx1007, D=>
--       nx940);
--    ix1006 : NAND21 port map ( Q=>nx1005, A=>count_5, B=>nx920);
--    reg_count_6 : DFC1 port map ( Q=>count_6, QN=>nx1007, C=>clk, D=>nx162, 
--       RN=>nx947);
--    ix177 : OAI221 port map ( Q=>nx176, A=>nx1011, B=>nx973, C=>nx1014, D=>
--       nx940);
--    ix169 : NOR40 port map ( Q=>nx168, A=>nx1007, B=>nx999, C=>nx978, D=>
--       nx997);
--    reg_count_7 : DFC1 port map ( Q=>count_7, QN=>nx1014, C=>clk, D=>nx176, 
--       RN=>nx947);
--    ix195 : OAI221 port map ( Q=>nx194, A=>nx1018, B=>nx1020, C=>nx1024, D=>
--       nx940);
--    ix1019 : AOI211 port map ( Q=>nx1018, A=>count_7, B=>nx168, C=>count_8);
--    ix1021 : NAND21 port map ( Q=>nx1020, A=>nx1022, B=>nx8);
--    ix1023 : NAND31 port map ( Q=>nx1022, A=>nx168, B=>count_8, C=>count_7);
--    reg_count_8 : DFC1 port map ( Q=>count_8, QN=>nx1024, C=>clk, D=>nx194, 
--       RN=>nx947);
--    ix555 : NAND41 port map ( Q=>nx554, A=>nx1027, B=>nx1040, C=>nx1034, D=>
--       nx1047);
--    ix221 : OAI221 port map ( Q=>nx220, A=>nx1030, B=>nx973, C=>nx1027, D=>
--       nx940);
--    reg_count_10 : DFC1 port map ( Q=>count_10, QN=>nx1027, C=>clk, D=>nx220, 
--       RN=>nx947);
--    ix215 : NOR21 port map ( Q=>nx214, A=>nx1034, B=>nx1022);
--    ix209 : OAI221 port map ( Q=>nx208, A=>nx1037, B=>nx973, C=>nx1034, D=>
--       nx940);
--    reg_count_9 : DFC1 port map ( Q=>OPEN, QN=>nx1034, C=>clk, D=>nx208, RN=>
--       nx947);
--    ix235 : OAI221 port map ( Q=>nx234, A=>nx1043, B=>nx973, C=>nx1040, D=>
--       nx940);
--    reg_count_11 : DFC1 port map ( Q=>count_11, QN=>nx1040, C=>clk, D=>nx234, 
--       RN=>nx947);
--    ix227 : NOR31 port map ( Q=>nx226, A=>nx1027, B=>nx1034, C=>nx1022);
--    ix1048 : NOR40 port map ( Q=>nx1047, A=>count_12, B=>count_13, C=>
--       count_14, D=>count_15);
--    ix253 : OAI311 port map ( Q=>nx252, A=>nx1051, B=>nx922, C=>nx973, D=>
--       nx1058);
--    ix1052 : AOI211 port map ( Q=>nx1051, A=>count_11, B=>nx226, C=>count_12
--    );
--    ix261 : NOR31 port map ( Q=>nx922, A=>nx1054, B=>nx1056, C=>nx1040);
--    ix1055 : NAND21 port map ( Q=>nx1054, A=>count_10, B=>nx214);
--    reg_count_12 : DFC1 port map ( Q=>count_12, QN=>nx1056, C=>clk, D=>nx252, 
--       RN=>nx947);
--    ix1059 : NAND21 port map ( Q=>nx1058, A=>count_12, B=>nx18);
--    ix19 : NOR21 port map ( Q=>nx18, A=>state, B=>start);
--    ix267 : OAI221 port map ( Q=>nx266, A=>nx1063, B=>nx973, C=>nx1065, D=>
--       nx940);
--    reg_count_13 : DFC1 port map ( Q=>count_13, QN=>nx1065, C=>clk, D=>nx266, 
--       RN=>nx947);
--    ix279 : OAI221 port map ( Q=>nx278, A=>nx1069, B=>nx973, C=>nx1073, D=>
--       nx940);
--    ix1072 : NAND21 port map ( Q=>nx1071, A=>count_13, B=>nx922);
--    reg_count_14 : DFC1 port map ( Q=>count_14, QN=>nx1073, C=>clk, D=>nx278, 
--       RN=>nx947);
--    ix293 : OAI221 port map ( Q=>nx292, A=>nx1077, B=>nx973, C=>nx1080, D=>
--       nx940);
--    ix285 : NOR21 port map ( Q=>nx284, A=>nx1073, B=>nx1071);
--    reg_count_15 : DFC1 port map ( Q=>count_15, QN=>nx1080, C=>clk, D=>nx292, 
--       RN=>nx947);
--    ix541 : NAND41 port map ( Q=>nx540, A=>nx1083, B=>nx1102, C=>nx1092, D=>
--       nx1109);
--    ix325 : OAI221 port map ( Q=>nx324, A=>nx1086, B=>nx973, C=>nx1083, D=>
--       nx940);
--    reg_count_17 : DFC1 port map ( Q=>count_17, QN=>nx1083, C=>clk, D=>nx324, 
--       RN=>nx947);
--    ix319 : NOR31 port map ( Q=>nx923, A=>nx1090, B=>nx1092, C=>nx1080);
--    ix1091 : NAND31 port map ( Q=>nx1090, A=>count_14, B=>count_13, C=>nx922
--    );
--    ix311 : OAI221 port map ( Q=>nx310, A=>nx1095, B=>nx1098, C=>nx1092, D=>
--       nx940);
--    ix1096 : AOI211 port map ( Q=>nx1095, A=>count_15, B=>nx284, C=>count_16
--    );
--    reg_count_16 : DFC1 port map ( Q=>count_16, QN=>nx1092, C=>clk, D=>nx310, 
--       RN=>nx947);
--    ix1099 : NAND21 port map ( Q=>nx1098, A=>nx1100, B=>nx8);
--    ix1101 : NAND31 port map ( Q=>nx1100, A=>nx284, B=>count_16, C=>count_15
--    );
--    ix337 : OAI221 port map ( Q=>nx336, A=>nx1105, B=>nx973, C=>nx1102, D=>
--       nx940);
--    reg_count_18 : DFC1 port map ( Q=>count_18, QN=>nx1102, C=>clk, D=>nx336, 
--       RN=>nx947);
--    ix331 : NOR21 port map ( Q=>nx330, A=>nx1083, B=>nx1100);
--    ix1110 : NOR40 port map ( Q=>nx1109, A=>count_19, B=>count_20, C=>
--       count_21, D=>count_22);
--    ix351 : OAI221 port map ( Q=>nx350, A=>nx1113, B=>nx973, C=>nx1116, D=>
--       nx940);
--    ix343 : NOR31 port map ( Q=>nx342, A=>nx1102, B=>nx1083, C=>nx1100);
--    reg_count_19 : DFC1 port map ( Q=>count_19, QN=>nx1116, C=>clk, D=>nx350, 
--       RN=>nx947);
--    ix369 : OAI211 port map ( Q=>nx368, A=>nx1120, B=>nx940, C=>nx1122);
--    reg_count_20 : DFC1 port map ( Q=>count_20, QN=>nx1120, C=>clk, D=>nx368, 
--       RN=>nx947);
--    ix1123 : NAND31 port map ( Q=>nx1122, A=>nx358, B=>nx1127, C=>nx8);
--    ix359 : OAI211 port map ( Q=>nx358, A=>nx1116, B=>nx1125, C=>nx1120);
--    ix1126 : NAND21 port map ( Q=>nx1125, A=>count_18, B=>nx330);
--    ix1128 : NAND31 port map ( Q=>nx1127, A=>nx342, B=>count_20, C=>count_19
--    );
--    ix383 : OAI221 port map ( Q=>nx382, A=>nx1131, B=>nx973, C=>nx1134, D=>
--       nx940);
--    ix377 : NOR31 port map ( Q=>nx924, A=>nx1125, B=>nx1120, C=>nx1116);
--    reg_count_21 : DFC1 port map ( Q=>count_21, QN=>nx1134, C=>clk, D=>nx382, 
--       RN=>nx947);
--    ix395 : OAI221 port map ( Q=>nx394, A=>nx1138, B=>nx973, C=>nx1142, D=>
--       nx940);
--    ix1141 : NAND21 port map ( Q=>nx1140, A=>count_21, B=>nx924);
--    reg_count_22 : DFC1 port map ( Q=>count_22, QN=>nx1142, C=>clk, D=>nx394, 
--       RN=>nx947);
--    ix529 : NAND21 port map ( Q=>nx528, A=>nx1145, B=>nx1180);
--    ix1146 : NOR40 port map ( Q=>nx1145, A=>count_23, B=>count_24, C=>
--       count_25, D=>count_26);
--    ix409 : OAI221 port map ( Q=>nx408, A=>nx1149, B=>nx973, C=>nx1152, D=>
--       nx940);
--    ix401 : NOR21 port map ( Q=>nx400, A=>nx1142, B=>nx1140);
--    reg_count_23 : DFC1 port map ( Q=>count_23, QN=>nx1152, C=>clk, D=>nx408, 
--       RN=>nx947);
--    ix427 : OAI221 port map ( Q=>nx426, A=>nx1156, B=>nx1158, C=>nx1162, D=>
--       nx940);
--    ix1157 : AOI211 port map ( Q=>nx1156, A=>count_23, B=>nx400, C=>count_24
--    );
--    ix1159 : NAND21 port map ( Q=>nx1158, A=>nx1160, B=>nx8);
--    ix1161 : NAND31 port map ( Q=>nx1160, A=>nx400, B=>count_24, C=>count_23
--    );
--    reg_count_24 : DFC1 port map ( Q=>count_24, QN=>nx1162, C=>clk, D=>nx426, 
--       RN=>nx947);
--    ix441 : OAI221 port map ( Q=>nx440, A=>nx1166, B=>nx973, C=>nx1171, D=>
--       nx940);
--    ix435 : NOR31 port map ( Q=>nx925, A=>nx1169, B=>nx1162, C=>nx1152);
--    ix1170 : NAND31 port map ( Q=>nx1169, A=>count_22, B=>count_21, C=>nx924
--    );
--    reg_count_25 : DFC1 port map ( Q=>count_25, QN=>nx1171, C=>clk, D=>nx440, 
--       RN=>nx947);
--    ix453 : OAI221 port map ( Q=>nx452, A=>nx1175, B=>nx973, C=>nx1178, D=>
--       nx940);
--    ix447 : NOR21 port map ( Q=>nx446, A=>nx1171, B=>nx1160);
--    reg_count_26 : DFC1 port map ( Q=>count_26, QN=>nx1178, C=>clk, D=>nx452, 
--       RN=>nx947);
--    ix1181 : NOR40 port map ( Q=>nx1180, A=>count_27, B=>count_28, C=>
--       count_29, D=>count_30);
--    ix467 : OAI221 port map ( Q=>nx466, A=>nx1184, B=>nx973, C=>nx1187, D=>
--       nx940);
--    ix459 : NOR31 port map ( Q=>nx458, A=>nx1178, B=>nx1171, C=>nx1160);
--    reg_count_27 : DFC1 port map ( Q=>count_27, QN=>nx1187, C=>clk, D=>nx466, 
--       RN=>nx947);
--    ix485 : OAI211 port map ( Q=>nx484, A=>nx1191, B=>nx940, C=>nx1193);
--    reg_count_28 : DFC1 port map ( Q=>count_28, QN=>nx1191, C=>clk, D=>nx484, 
--       RN=>nx947);
--    ix1194 : NAND31 port map ( Q=>nx1193, A=>nx474, B=>nx1198, C=>nx8);
--    ix475 : OAI211 port map ( Q=>nx474, A=>nx1187, B=>nx1196, C=>nx1191);
--    ix1197 : NAND21 port map ( Q=>nx1196, A=>count_26, B=>nx446);
--    ix1199 : NAND31 port map ( Q=>nx1198, A=>nx458, B=>count_28, C=>count_27
--    );
--    ix499 : OAI221 port map ( Q=>nx498, A=>nx1202, B=>nx973, C=>nx1205, D=>
--       nx940);
--    ix493 : NOR31 port map ( Q=>nx926, A=>nx1196, B=>nx1191, C=>nx1187);
--    reg_count_29 : DFC1 port map ( Q=>count_29, QN=>nx1205, C=>clk, D=>nx498, 
--       RN=>nx947);
--    ix511 : OAI221 port map ( Q=>nx510, A=>nx1209, B=>nx973, C=>nx1212, D=>
--       nx940);
--    ix505 : NOR21 port map ( Q=>nx504, A=>nx1205, B=>nx1198);
--    reg_count_30 : DFC1 port map ( Q=>count_30, QN=>nx1212, C=>clk, D=>nx510, 
--       RN=>nx947);
--    ix1215 : NAND21 port map ( Q=>nx1214, A=>count_2, B=>nx18);
--    ix561 : NAND41 port map ( Q=>nx560, A=>nx999, B=>nx1007, C=>nx1014, D=>
--       nx1024);
--    ix1218 : NOR40 port map ( Q=>nx1217, A=>count_17, B=>count_18, C=>
--       count_16, D=>nx534);
--    ix535 : NAND41 port map ( Q=>nx534, A=>nx1116, B=>nx1120, C=>nx1134, D=>
--       nx1142);
--    ix9 : IMUX21 port map ( Q=>nx8, A=>nx945, B=>aboveth_EXMPLR, S=>state);
--    ix964 : XOR21 port map ( Q=>nx963, A=>nx961, B=>start);
--    ix974 : IMUX22 port map ( Q=>nx973, A=>start, B=>nx975, S=>state);
--    ix995 : XOR21 port map ( Q=>nx994, A=>nx999, B=>nx920);
--    ix1004 : XNR21 port map ( Q=>nx1003, A=>nx1007, B=>nx1005);
--    ix1012 : XOR21 port map ( Q=>nx1011, A=>nx1014, B=>nx168);
--    ix1031 : XOR21 port map ( Q=>nx1030, A=>nx1027, B=>nx214);
--    ix1038 : XNR21 port map ( Q=>nx1037, A=>nx1034, B=>nx1022);
--    ix1044 : XOR21 port map ( Q=>nx1043, A=>nx1040, B=>nx226);
--    ix1064 : XOR21 port map ( Q=>nx1063, A=>nx1065, B=>nx922);
--    ix1070 : XNR21 port map ( Q=>nx1069, A=>nx1073, B=>nx1071);
--    ix1078 : XOR21 port map ( Q=>nx1077, A=>nx1080, B=>nx284);
--    ix1087 : XOR21 port map ( Q=>nx1086, A=>nx1083, B=>nx923);
--    ix1106 : XOR21 port map ( Q=>nx1105, A=>nx1102, B=>nx330);
--    ix1114 : XOR21 port map ( Q=>nx1113, A=>nx1116, B=>nx342);
--    ix1132 : XOR21 port map ( Q=>nx1131, A=>nx1134, B=>nx924);
--    ix1139 : XNR21 port map ( Q=>nx1138, A=>nx1142, B=>nx1140);
--    ix1150 : XOR21 port map ( Q=>nx1149, A=>nx1152, B=>nx400);
--    ix1167 : XOR21 port map ( Q=>nx1166, A=>nx1171, B=>nx925);
--    ix1176 : XOR21 port map ( Q=>nx1175, A=>nx1178, B=>nx446);
--    ix1185 : XOR21 port map ( Q=>nx1184, A=>nx1187, B=>nx458);
--    ix1203 : XOR21 port map ( Q=>nx1202, A=>nx1205, B=>nx926);
--    ix1210 : XOR21 port map ( Q=>nx1209, A=>nx1212, B=>nx504);
-- end Unconstrained ;
-- 
-- architecture Constrained of Counter is
--    signal aboveth_EXMPLR, count_3, state, nx8, nx18, count_2, count_1, 
--       count_0, nx36, nx52, nx68, nx84, nx206, nx211, nx213, nx215, nx217, 
--       nx221, nx223, nx225, nx228, nx230, nx233, nx236, nx238, nx240, nx244, 
--       nx246, nx248, nx250: std_logic ;
-- 
-- begin
--    aboveth <= aboveth_EXMPLR ;
--    ix93 : OAI311 port map ( Q=>aboveth_EXMPLR, A=>nx206, B=>nx225, C=>nx230, 
--       D=>nx246);
--    ix53 : OAI311 port map ( Q=>nx52, A=>nx206, B=>state, C=>start, D=>nx217
--    );
--    ix212 : CLKIN1 port map ( Q=>nx211, A=>start);
--    reg_state : DFC1 port map ( Q=>state, QN=>nx213, C=>clk, D=>nx8, RN=>
--       nx215);
--    ix216 : CLKIN1 port map ( Q=>nx215, A=>reset);
--    ix218 : OAI2111 port map ( Q=>nx217, A=>count_0, B=>count_1, C=>nx228, D
--       =>nx8);
--    ix37 : OAI221 port map ( Q=>nx36, A=>state, B=>nx221, C=>aboveth_EXMPLR, 
--       D=>nx223);
--    ix224 : NAND21 port map ( Q=>nx223, A=>nx225, B=>state);
--    reg_count_0 : DFC1 port map ( Q=>count_0, QN=>nx225, C=>clk, D=>nx36, RN
--       =>nx215);
--    reg_count_1 : DFC1 port map ( Q=>count_1, QN=>nx206, C=>clk, D=>nx52, RN
--       =>nx215);
--    ix229 : NAND21 port map ( Q=>nx228, A=>count_1, B=>count_0);
--    ix69 : OAI311 port map ( Q=>nx68, A=>nx233, B=>nx236, C=>nx240, D=>nx250
--    );
--    ix234 : AOI211 port map ( Q=>nx233, A=>count_1, B=>count_0, C=>count_2);
--    reg_count_2 : DFC1 port map ( Q=>count_2, QN=>nx230, C=>clk, D=>nx68, RN
--       =>nx215);
--    ix239 : NOR21 port map ( Q=>nx238, A=>nx240, B=>count_3);
--    ix241 : NOR31 port map ( Q=>nx240, A=>nx206, B=>nx225, C=>nx230);
--    ix85 : OAI221 port map ( Q=>nx84, A=>nx244, B=>nx236, C=>nx246, D=>nx248
--    );
--    reg_count_3 : DFC1 port map ( Q=>count_3, QN=>nx246, C=>clk, D=>nx84, RN
--       =>nx215);
--    ix249 : NAND21 port map ( Q=>nx248, A=>nx213, B=>nx211);
--    ix251 : NAND21 port map ( Q=>nx250, A=>count_2, B=>nx18);
--    ix19 : NOR21 port map ( Q=>nx18, A=>state, B=>start);
--    ix9 : IMUX21 port map ( Q=>nx8, A=>nx211, B=>aboveth_EXMPLR, S=>state);
--    ix222 : XOR21 port map ( Q=>nx221, A=>nx225, B=>start);
--    ix237 : IMUX21 port map ( Q=>nx236, A=>start, B=>nx238, S=>state);
--    ix245 : XOR21 port map ( Q=>nx244, A=>nx246, B=>nx240);
-- end Constrained ;
