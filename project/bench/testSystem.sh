#!/bin/bash

## nettoyage
if [ -d ../libs/LIB_SYSTEM_BENCH ] 
then /bin/rm -rf ../libs/LIB_SYSTEM_BENCH
fi

## creation de la librairie de travail
vlib ../libs/LIB_SYSTEM_BENCH


## compilation
vcom -work LIB_SYSTEM_BENCH testSystem.vhd
