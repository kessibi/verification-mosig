library IEEE;
library LIB_SYSTEM;
use IEEE.std_logic_1164.all;

entity testSystem is
end testSystem;

architecture simulation1 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 1: Unsuccessful search
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 55 ns, '1' after 155 ns;
  s_findfood <= '0';
  s_lostfood <= '0';
  s_closetofood <= '0';
  s_success <= '0';
  s_scantimeup <= '0';
end simulation1;

architecture simulation2 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;
    
    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 2: Successful search
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 55 ns, '1' after 105 ns;
  s_findfood <= '0', '1' after 55 ns, '0' after 65 ns;
  s_lostfood <= '0'; s_closetofood <= '0', '1' after 85 ns, '0' after 95 ns; s_success <= '0', '1' after 95 ns, '0' after 105 ns, '1' after 115 ns, '0' after 125 ns;
  s_scantimeup <= '0';
end simulation2;

architecture simulation3 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;
    
    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 3: Unrecovered lost food
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 55 ns, '1' after 155 ns;
  s_findfood <= '0', '1' after 55 ns, '0' after 65 ns;
  s_lostfood <= '0', '1' after 65 ns;
  s_closetofood <= '0';
  s_success <= '0';
  s_scantimeup <= '0';
end simulation3;

architecture simulation4 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 4: Losing food but finding it again
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 45 ns, '1' after 155 ns;
  s_findfood <= '0', '1' after 55 ns, '0' after 65 ns, '1' after 85 ns, '0' after 95 ns;
  s_lostfood <= '0', '1' after 65 ns, '0' after 75 ns;
  s_closetofood <= '0', '1' after 95 ns, '0' after 105 ns;
  s_success <= '0', '1' after 115 ns, '0' after 125 ns, '1' after 155 ns, '0' after 165 ns;
  s_scantimeup <= '0', '1' after 75 ns, '0' after 85 ns;
end simulation4;

architecture simulation5 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 5: Lose food, find food, not reach food
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 45 ns, '1' after 165 ns;
  s_findfood <= '0', '1' after 55 ns, '0' after 65 ns, '1' after 75 ns, '0' after 85 ns;
  s_lostfood <= '0', '1' after 65 ns, '0' after 75 ns;
  s_closetofood <= '0';
  s_success <= '0';
  s_scantimeup <= '0';
end simulation5;

architecture simulation6 of testSystem is
    component System is
    port(reset, clk, athome, findfood, lostfood, closetofood,
         success, scantimeup: in std_logic;
         food: out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
           s_success, s_scantimeup : std_logic := '0';
    
    signal orig_food : std_logic := '0';
    signal classic_food : std_logic := '0';
    signal gray_food : std_logic := '0';
    signal onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin
  SOrig : entity LIB_SYSTEM.System(Orig)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => orig_food);

  SClassic : entity LIB_SYSTEM.System(Classic)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => classic_food);

  SGray : entity LIB_SYSTEM.System(Gray)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => gray_food);

  SOneHot : entity LIB_SYSTEM.System(OneHot)
    port map (reset => s_reset, clk => s_clk, athome => s_athome,
              findfood => s_findfood, lostfood => s_lostfood,
              closetofood => s_closetofood, success => s_success,
              scantimeup => s_scantimeup, food => onehot_food);

  -- Simulation 6: Lose food, find food, not reach food
  s_clk <= not s_clk after 5 ns;
  s_reset <= '1', '0' after 3 ns;
  s_athome <= '1', '0' after 45 ns, '1' after 155 ns;
  s_findfood <= '0', '1' after 65 ns, '0' after 75 ns;
  s_lostfood <= '0', '1' after 75 ns, '0' after 85 ns;
  s_closetofood <= '0';
  s_success <= '0';
  s_scantimeup <= '0', '1' after 95 ns, '0' after 105 ns;
end simulation6;
