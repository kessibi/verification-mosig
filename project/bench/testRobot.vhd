library IEEE;
library LIB_ROBOT;
use IEEE.std_logic_1164.all;

entity testRobot is
end testRobot;

architecture simulation1 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);


    -- Simulation 1: Unsuccessful search
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 55 ns, '1' after 155 ns;
    s_findfood <= '0';
    s_lostfood <= '0';
    s_closetofood <= '0';
    s_success <= '0';
    s_scantimeup <= '0';

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0', '1' after 145 ns, '0' after 155 ns;
end simulation1;

architecture simulation2 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);


    -- Simulation 2: Successful search
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 55 ns, '1' after 105 ns;
    s_findfood <= '0', '1' after 55 ns, '0' after 65 ns;
    s_lostfood <= '0'; s_closetofood <= '0', '1' after 85 ns, '0' after 95 ns; s_success <= '0', '1' after 95 ns, '0' after 105 ns, '1' after 115 ns, '0' after 125 ns;
    s_scantimeup <= '0';

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0';
end simulation2;

architecture simulation3 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);


    -- Simulation 3: Unrecovered lost food
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 55 ns, '1' after 155 ns;
    s_findfood <= '0', '1' after 55 ns, '0' after 65 ns;
    s_lostfood <= '0', '1' after 65 ns;
    s_closetofood <= '0';
    s_success <= '0';
    s_scantimeup <= '0';

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0', '1' after 145 ns, '0' after 155 ns;
end simulation3;

architecture simulation4 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);

    -- Simulation 4: Losing food but finding it again after scantimeup
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 45 ns, '1' after 155 ns;
    s_findfood <= '0', '1' after 55 ns, '0' after 65 ns, '1' after 85 ns, '0' after 95 ns;
    s_lostfood <= '0', '1' after 65 ns, '0' after 75 ns;
    s_closetofood <= '0', '1' after 95 ns, '0' after 105 ns;
    s_success <= '0', '1' after 115 ns, '0' after 125 ns, '1' after 155 ns, '0' after 165 ns;
    s_scantimeup <= '0', '1' after 75 ns, '0' after 85 ns;

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0';
end simulation4;

architecture simulation5 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);

    -- simulation 5: lose food, find food, not reach food
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 45 ns, '1' after 165 ns;
    s_findfood <= '0', '1' after 55 ns, '0' after 65 ns, '1' after 75 ns, '0' after 85 ns;
    s_lostfood <= '0', '1' after 65 ns, '0' after 75 ns;
    s_closetofood <= '0';
    s_success <= '0';
    s_scantimeup <= '0';

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0', '1' after 145 ns, '0' after 155 ns;
end simulation5;


architecture simulation6 of testRobot is
    component Robot is
    port(reset, clk, athome, findfood, lostfood, closetofood, success,
    aboverestth, abovesearchth, scantimeup : in std_logic;
    rest, search, food : out std_logic);
    end component;

    signal s_clk : std_logic := '1';
    signal s_reset, s_athome, s_findfood, s_lostfood, s_closetofood,
    s_success, s_aboverestth, s_abovesearchth, s_scantimeup : std_logic := '0';
    signal orig_rest, orig_search, orig_food : std_logic := '0';
    signal classic_rest, classic_search, classic_food : std_logic := '0';
    signal gray_rest, gray_search, gray_food : std_logic := '0';
    signal onehot_rest, onehot_search, onehot_food : std_logic := '0';

-- PSL default clock is (s_clk'event and s_clk = '1');
-- PSL property equal_outputs is
-- always(
--   ((orig_rest = classic_rest) and (orig_rest = gray_rest) and (orig_rest= onehot_rest))
--   and
--   ((orig_search = classic_search) and (orig_search = gray_search) and (orig_search = onehot_search))
--   and
--   ((orig_food = classic_food) and (orig_food = gray_food) and (orig_food = onehot_food))
-- );
-- PSL assert equal_outputs;

begin

    ROrig : entity LIB_ROBOT.Robot(Orig) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => orig_rest,
                        search => orig_search, food => orig_food, reset => s_reset);

    RClassic : entity LIB_ROBOT.Robot(Classic) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => classic_rest,
                        search => classic_search, food => classic_food, reset => s_reset);

    RGray : entity LIB_ROBOT.Robot(Gray) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => gray_rest,
                        search => gray_search, food => gray_food, reset => s_reset);

    ROneHot : entity LIB_ROBOT.Robot(OneHot) port map (clk => s_clk, athome => s_athome, findfood => s_findfood,
                        lostfood => s_lostfood, closetofood => s_closetofood,
                        success => s_success, aboverestth => s_aboverestth,
                        abovesearchth => s_abovesearchth,
                        scantimeup => s_scantimeup, rest => onehot_rest,
                        search => onehot_search, food => onehot_food, reset => s_reset);

    -- Simulation 6: Lose food, find food, not reach food
    s_clk <= not s_clk after 5 ns;
    s_reset <= '1', '0' after 3 ns;
    s_athome <= '1', '0' after 45 ns, '1' after 155 ns;
    s_findfood <= '0', '1' after 65 ns, '0' after 75 ns;
    s_lostfood <= '0', '1' after 75 ns, '0' after 85 ns;
    s_closetofood <= '0';
    s_success <= '0';
    s_scantimeup <= '0', '1' after 95 ns, '0' after 105 ns;

    s_aboverestth <= '0', '1' after 45 ns, '0' after 55 ns;
    s_abovesearchth <= '0', '1' after 145 ns, '0' after 155 ns;
end simulation6;
