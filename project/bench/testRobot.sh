#!/bin/bash

## nettoyage
if [ -d ../libs/LIB_ROBOT_BENCH ] 
then /bin/rm -rf ../libs/LIB_ROBOT_BENCH
fi

## creation de la librairie de travail
vlib ../libs/LIB_ROBOT_BENCH


## compilation
vcom -work LIB_ROBOT_BENCH testRobot.vhd
